from owlready2 import *
import rdflib
prefix_of_ontology="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"
onto = get_ontology("ttt.owl")
onto.load()

#запрос на select
graph = default_world.as_rdflib_graph()
r = list(graph.query("""PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX owl: <http://www.w3.org/2002/07/owl#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	PREFIX learn: """+ "<" +prefix_of_ontology + ">" + 
	"""SELECT ?subject ?object ?object2
		WHERE { ?subject rdfs:domain ?object .
	        	       ?subject rdfs:range ?object2 .
	}"""))

##################################     Создание триплетов    ###################################

#Создание класса
#with onto:
#	graph.add((rdflib.URIRef(prefix_of_ontology+"Z"), rdflib.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdflib.URIRef("http://www.w3.org/2002/07/owl#Class")))

#Создание подкласса
#with onto:
#	graph.add((rdflib.URIRef(prefix_of_ontology+"subclassZ"), rdflib.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdflib.URIRef("http://www.w3.org/2002/07/owl#Class")))
#	graph.add((rdflib.URIRef(prefix_of_ontology+"subclassZ"), rdflib.URIRef("http://www.w3.org/2000/01/rdf-schema#subClassOf"), rdflib.URIRef(prefix_of_ontology+"Z")))

#Создание экземпляра класса
#with onto:
#	graph.add((rdflib.URIRef(prefix_of_ontology+"instanseZ"), rdflib.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdflib.URIRef("http://www.w3.org/2002/07/owl#NamedIndividual")))
#	graph.add((rdflib.URIRef(prefix_of_ontology+"instanseZ"), rdflib.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdflib.URIRef(prefix_of_ontology+"subclassZ")))

#Создание связи через domain range в свойстве
#with onto:
#	graph.add((rdflib.URIRef(prefix_of_ontology+"_z2_следует_за_z1_"), rdflib.URIRef("http://www.w3.org/2000/01/rdf-schema#range"), rdflib.URIRef(prefix_of_ontology+"z1")))
#	graph.add((rdflib.URIRef(prefix_of_ontology+"_z2_следует_за_z1_"), rdflib.URIRef("http://www.w3.org/2000/01/rdf-schema#domain"), rdflib.URIRef(prefix_of_ontology+"z2")))

#Создание свойства
#with onto:
#	graph.add((rdflib.URIRef(prefix_of_ontology+"_еще_одно_свойство_"), rdflib.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdflib.URIRef("http://www.w3.org/2002/07/owl#ObjectProperty")))
#Создание подсвойства
#with onto:
#	graph.add((rdflib.URIRef(prefix_of_ontology+"_еще_одно_подсвойство_"), rdflib.URIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), rdflib.URIRef("http://www.w3.org/2002/07/owl#ObjectProperty")))
#	graph.add((rdflib.URIRef(prefix_of_ontology+"_еще_одно_подсвойство_"), rdflib.URIRef("http://www.w3.org/2000/01/rdf-schema#subPropertyOf"), rdflib.URIRef(rdflib.URIRef(prefix_of_ontology+"_еще_одно_свойство_"))))


###################################       Работа с выводом            ##################################
#print("classes:")
#for i in onto.classes():
#    print(i, i.storid, onto.get_parents_of(i), onto.get_children_of(i))
#print("data_properties:")
#for i in onto.data_properties():
#    print(i, i.storid)
#print("different_individuals:")
#for i in onto.different_individuals():
#    print(i, i.storid)
#print("individuals:")
#for i in onto.individuals():
#    print(i, i.storid)
#print("disjoint_properties:")
#for i in onto.disjoint_properties():
#    print(i, i.storid)
#print("object_properties:")
#for i in onto.object_properties():
#    print(i, i.storid)
#print("triples:")
#print(onto.get_triples())

#перевод из кортежей с storid в iri

a = []
for i in onto.get_triples():
    try:
        a.append((default_world._unabbreviate(i[0]),default_world._unabbreviate(i[1]),default_world._unabbreviate(i[2])))
    except:
        pass
for z in a: 
	print(z)

#получение дампа триплетов в виде iri
#print("search")

#onto.search(iri="*")
#onto.graph.dump()
#for i in r:
#    print(i)

#onto.search(".")
#onto.save(file = "./ttt.owl", format = "rdfxml")

