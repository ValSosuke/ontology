from django.shortcuts import redirect
class SimpleMiddleware(object):
    
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if(request.user.is_authenticated==False):
            if(request.path_info.find("/admin/")<0):
                request.path_info="/admin/"
                response = self.get_response(request)
            else:
                response = self.get_response(request)
        else:
            response = self.get_response(request)
        return response
    
                        
