# Generated by Django 3.1 on 2020-11-17 09:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0002_auto_20201109_1554'),
    ]

    operations = [
        migrations.AddField(
            model_name='simplerulesforontology',
            name='right_direction_first_role_first',
            field=models.BooleanField(default=True),
        ),
    ]
