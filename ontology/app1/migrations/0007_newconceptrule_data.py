# Generated by Django 3.1.5 on 2021-03-23 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0006_subprocessrule'),
    ]

    operations = [
        migrations.AddField(
            model_name='newconceptrule',
            name='data',
            field=models.DateTimeField(db_column='data', default=None),
            preserve_default=False,
        ),
    ]
