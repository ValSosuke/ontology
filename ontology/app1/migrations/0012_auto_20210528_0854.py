# Generated by Django 3.1 on 2021-05-28 08:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0011_triplets_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newconceptrule',
            name='ontology',
            field=models.ForeignKey(blank=True, db_column='ontology', on_delete=django.db.models.deletion.CASCADE, to='app1.createdontology'),
        ),
        migrations.AlterField(
            model_name='simplerulesforontology',
            name='ontology',
            field=models.ForeignKey(blank=True, db_column='ontology', on_delete=django.db.models.deletion.CASCADE, to='app1.createdontology'),
        ),
        migrations.AlterField(
            model_name='subprocessrule',
            name='ontology',
            field=models.ForeignKey(blank=True, db_column='ontology', on_delete=django.db.models.deletion.CASCADE, to='app1.createdontology'),
        ),
    ]
