from django.core.checks.messages import Error
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views import View
from .forms import UploadFileForm
from django.conf import settings
import os
from app1.models import *

from django.conf import settings
from datetime import datetime
import json
from django.db.models import Q
from django.core.exceptions import EmptyResultSet, ObjectDoesNotExist
import json
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import random

# Create your views here.

def handle_uploaded_file(f):
    
    with open('ontology/app1/ontology_file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


#####################################################################################################################
###########################################     INDEX PAGE        ###################################################
#####################################################################################################################

class IndexPage(View):
    def get(self, request):
        form = UploadFileForm()
        ontologes = CreatedOntology.objects.values('author', 'name', 'description', 'data', 'author__avatar', 'author__email')

        return render(request, 'app1/index.html', {'form': form, 'ontologes': ontologes},)
    def post(self, request):
        print("post:",request.POST)
        name = request.POST["name"]
        description = request.POST["description"]
        names_onto=list(CreatedOntology.objects.values_list("name",flat=True))
        if(name in names_onto):
            pass
        else:
            a = CreatedOntology(author = request.user, name=name, description=description, data = datetime.now())    
            a.save()
        form = UploadFileForm()
        ontologes = CreatedOntology.objects.values('author', 'name', 'description', 'data', 'author__avatar', 'author__email')
        return render(request, 'app1/index.html', {'form': form, 'ontologes': ontologes},)
        """
        path_ontology_absolute_path = os.path.join(settings.PATH_TO_ONTOLOGE_FILES, request.POST[""]) #Создания пути для сохранения онтологии
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponse('Good Load')
        else: 
            return HttpResponse('Bad load')
        """

#########################################################################################################
#################################           ENVIROMENT  PAGE        #####################################
#########################################################################################################

class OntologyCreationEnvironment(View): #Отдается начальная страница
    def get(self, request):
        request.session['ontology_name']=request.GET.get("ontology")
        
        return render(request, 'app1/creation_enviroment_ontology.html', \
            {
                "listOfClasses": sorted(getAllClasses(ontology = CreatedOntology.objects.get(name=request.session['ontology_name']))), \
                "listOfProperty": sorted(getAllProperty(ontology = CreatedOntology.objects.get(name=request.session['ontology_name']))), \
                "listOfConcepts": sorted(getConcepts(ontology = CreatedOntology.objects.get(name=request.session['ontology_name']))), \
                "listOfIndividuals": sorted(getAllIndividuals(ontology = CreatedOntology.objects.get(name=request.session['ontology_name']))), \
                "listSimpleRules": getAllSimpleRules(ontology = CreatedOntology.objects.get(name=request.session['ontology_name'])), \
                "listNewConceptRules": getAllNewConceptRules(ontology = CreatedOntology.objects.get(name=request.session['ontology_name'])), \
                "listSubProcessRules": getAllSubProcessRules(ontology = CreatedOntology.objects.get(name=request.session['ontology_name'])), \
                "listPropertyConcepts": sorted(getPropertyConcepts(ontology = CreatedOntology.objects.get(name=request.session['ontology_name']))) \
            } )

#########################################################################################################
#################################           Применение простых правил       #############################
#########################################################################################################
def applicationSimpleRule(mail,ontology, list_num_of_rules):
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    objects_simple_rules = SimpleRulesForOntology.objects.filter(ontology=objectOntology) #   Получение всех объеков для нумераций
    list_of_ret=[]
    for num in list_num_of_rules: # Проход по всем выбранным правилам
        list_of_object_c_1 = [i for i in list(Triplets.objects.filter(Q(ontology = objectOntology),Q(object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), Q(object_destanation= "http://www.w3.org/2002/07/owl#Class") | Q(object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual")).values_list("object_source", flat=True)) if (getConceptOfObject(i, objectOntology).split('#')[1] == objects_simple_rules[num].first_concept)] #Получение всех объектов первого концепта из парвила 
        list_of_object_c_2 = [i for i in list(Triplets.objects.filter(Q(ontology = objectOntology),Q(object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), Q(object_destanation= "http://www.w3.org/2002/07/owl#Class") | Q(object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual")).values_list("object_source", flat=True)) if (getConceptOfObject(i, objectOntology).split('#')[1] == objects_simple_rules[num].second_concept)] #Получение всех объектов второго концепта из парвила
        list_of_object_c_3 = [i for i in list(Triplets.objects.filter(Q(ontology = objectOntology),Q(object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), Q(object_destanation= "http://www.w3.org/2002/07/owl#Class") | Q(object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual")).values_list("object_source", flat=True)) if (getConceptOfObject(i, objectOntology).split('#')[1] == objects_simple_rules[num].third_concept)] #Получение всех объектов третьего концепта из парвила 
        
        list_of_property1 = [objects_simple_rules[num].first_role_second]
        list_of_property1 += getAllSubpropertyOfProperty(ontology, objects_simple_rules[num].first_role_second) #Получение свойства из правила и его подсвойств
        
        for i in list_of_property1:

            prop_domain = list(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + i, object_property= "http://www.w3.org/2000/01/rdf-schema#domain").values_list("object_destanation", flat = True)) #Получение всех доменов первого отношения
            prop_range = list(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + i, object_property= "http://www.w3.org/2000/01/rdf-schema#range").values_list("object_destanation", flat=True)) #Получение всех ренжев второго свойства
            
            o_c_1 = []
            o_c_2 = []
            o_c_3 = []

            if (len(prop_domain) != 0 and len(prop_range) != 0): # если у данного свойства существуют связи
                if(objects_simple_rules[num].right_direction_first_role_first == True): #Если первая стрелка в право
                    o_c_1.extend(list(set(list_of_object_c_1) & set(prop_domain))) # если есть совпадения в domain
                    o_c_2.extend(list(set(list_of_object_c_2) & set(prop_range))) # если есть совпадения в range
                else:
                    o_c_1.extend(list(set(list_of_object_c_1) & set(prop_range))) # если есть совпадения в domain
                    o_c_2.extend(list(set(list_of_object_c_2) & set(prop_domain))) # если есть совпадения в range
            else: continue
            if (len(o_c_1)!=0 and len(o_c_2)!=0): # Если есть объекты у 2 концепта, которые входят в range свойства 1
                
                list_of_property2 = [objects_simple_rules[num].second_role_third]
                list_of_property2 += getAllSubpropertyOfProperty(ontology, objects_simple_rules[num].second_role_third)
                buf = o_c_2.copy()
                for j in list_of_property2:

                    prop_domain = list(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + j, object_property= "http://www.w3.org/2000/01/rdf-schema#domain").values_list("object_destanation", flat = True))
                    prop_range = list(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + j, object_property= "http://www.w3.org/2000/01/rdf-schema#range").values_list("object_destanation", flat=True))
                    
                    o_c_2.clear()
                    o_c_3.clear()
                    if (len(prop_domain) != 0 and len(prop_range) != 0): # если у данного свойства существуют связи
                        if(objects_simple_rules[num].right_direction_first_role_second == True): #Если вторая стрелка в право
                            o_c_2.extend(list(set(list_of_object_c_2) & set(prop_domain))) # если есть совпадения в domain
                            o_c_3.extend(list(set(list_of_object_c_3) & set(prop_range)))
                        else:
                            o_c_2.extend(list(set(list_of_object_c_2) & set(prop_range))) # если есть совпадения в domain
                            o_c_3.extend(list(set(list_of_object_c_3) & set(prop_domain)))
                    if(len(o_c_3)!=0 and o_c_2 == buf):
                        #Если рузультирующий вправо или лево
                        if(objects_simple_rules[num].right_direction_transitive_node==True):
                            # Если вправо то достраиваем связь от o_c_1 к o_c_3
                            for it_o_c_1 in o_c_1:
                                for it_o_c_3 in o_c_3:
                                    #addRelations(mail = settings.ROBOT_MAIL, ontology = ontology, first_object= it_o_c_1.split("#")[1], property_object=objects_simple_rules[num].transitive_node, second_object=it_o_c_3.split("#")[1])
                                    if ( addSubProperty(mail=settings.ROBOT_MAIL, ontology=ontology, propertyname= objects_simple_rules[num].transitive_node, subpropertyname= it_o_c_1.split("#")[1] +"_" + objects_simple_rules[num].transitive_node + "_" + it_o_c_3.split("#")[1])==True and \
                                        addDomainProperty(mail=settings.ROBOT_MAIL, ontology = ontology, propertyname = it_o_c_1.split("#")[1] + "_" + objects_simple_rules[num].transitive_node + "_" + it_o_c_3.split("#")[1], domainname=it_o_c_1.split("#")[1])==True and \
                                        addRangeProperty(mail=settings.ROBOT_MAIL, ontology = ontology, propertyname=it_o_c_1.split("#")[1] + "_" + objects_simple_rules[num].transitive_node + "_" + it_o_c_3.split("#")[1], rangename=it_o_c_3.split("#")[1])==True):
                                        
                                        list_of_ret.append((it_o_c_1.split("#")[1],it_o_c_3.split("#")[1]))
                                    else: 
                                        pass
                        else:
                            # Если влево то достраиваем связь от o_c_3 к o_c_1
                            for it_o_c_1 in o_c_1:
                                for it_o_c_3 in o_c_3:                            
                                    #addRelations(mail = settings.ROBOT_MAIL, ontology = ontology, first_object= it_o_c_3.split("#")[1], property_object=objects_simple_rules[num].transitive_node, second_object=it_o_c_1.split("#")[1])
                                    if ( addSubProperty(mail=settings.ROBOT_MAIL, ontology=ontology, propertyname= objects_simple_rules[num].transitive_node, subpropertyname= it_o_c_1.split("#")[1] +"_" + objects_simple_rules[num].transitive_node + "_" + it_o_c_3.split("#")[1])==True and \
                                        addDomainProperty(mail=settings.ROBOT_MAIL, ontology = ontology, propertyname = it_o_c_1.split("#")[1] + "_" + objects_simple_rules[num].transitive_node + "_" + it_o_c_3.split("#")[1], domainname=it_o_c_3.split("#")[1])==True and \
                                        addRangeProperty(mail=settings.ROBOT_MAIL, ontology = ontology, propertyname=it_o_c_1.split("#")[1] + "_" + objects_simple_rules[num].transitive_node + "_" + it_o_c_3.split("#")[1], rangename=it_o_c_1.split("#")[1])==True):
                                        
                                        list_of_ret.append((it_o_c_1.split("#")[1],it_o_c_3.split("#")[1]))
                                    else: 
                                        pass
            else: continue
    return {"result":True, "list_of_result":list_of_ret}
def deleteSimpleRule(ontology, pk):
    SimpleRulesForOntology.objects.get(pk=int(pk)).delete();
    return True

#########################################################################################################
#################################          Построение нового концепта       #############################
#########################################################################################################

def applicationNewConceptRule(mail,ontology, list_num_of_rules):
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    objects_new_concept_rules = NewConceptRule.objects.filter(ontology=objectOntology) 
    for num in list_num_of_rules:
        k1 = objects_new_concept_rules[num].k1
        k3 = objects_new_concept_rules[num].k3
        p2 = objects_new_concept_rules[num].p2
        addSubProperty(mail=mail, ontology=ontology, propertyname= p2, subpropertyname="[existential quantifier] "+p2) #Создание квантера существования
        k1_subclasses = [i["name"] for i in getStructure(action="Class", ontology=objectOntology) if i["parent"]==k1] #Получение списка подклассов класса k1
        #Создание подклассов для класса k3 k3[k1] и добавление связи p2
        list_of_ret=[]
        for i in k1_subclasses:
            #Подкласс Колесо[Автомобиля Сергея]
            if(addSubClass(mail=settings.ROBOT_MAIL, ontology=ontology, classname=k3, subclassname="{}[{}]".format(k3,i))):
                list_of_ret.append("{}[{}]".format(k3,i))
            else:
                pass
            #Подсвойство свойства состоит из состоит_из_АС_К[АС] 
            addSubProperty(mail=settings.ROBOT_MAIL, ontology=ontology, propertyname=p2, subpropertyname="{}_{}_{}".format(i,p2,"{}[{}]".format(k3,i)))
            #Связь Автомобиль Сергея Колесо[Автомобиля Сергея] через свойство АС_состоит_из_К[АС]
            addRelations(mail=settings.ROBOT_MAIL, ontology=ontology, first_object=i, property_object="{}_{}_{}".format(i,p2,"{}[{}]".format(k3,i)), second_object="{}[{}]".format(k3,i)) 
            #addDomainProperty(mail=settings.ROBOT_MAIL, ontology=ontology, )
            # Квантер существования
            
            # Существуют Автомобили, состоящие из Колесо[Автомоиля Сергея]
            addRelations(mail=settings.ROBOT_MAIL, ontology=ontology, first_object=k1, property_object="[existential quantifier] " \
                + p2, second_object= "{}[{}]".format(k3,i))
            # Автомобиль Сергея состоит из Колесо
            addRelations(mail=settings.ROBOT_MAIL, ontology=ontology, first_object=i, property_object="{}_{}_{}".format(i,p2,"{}[{}]".format(k3,i)), second_object= k3) 
        return {"result":True, "list_of_result":list_of_ret}
def deleteNewConceptRule(pk):
    NewConceptRule.objects.get(pk=int(pk)).delete();
    return True
#########################################################################################################
#################################          Вывод подпроцессов               #############################
#########################################################################################################

def applicationSubProcessRule(mail,ontology, list_num_of_rules):
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    list_of_ret=[]
    for num_of_rules in list_num_of_rules:
        p2 = SubProcessRule.objects.filter(ontology=objectOntology)[num_of_rules].p2
        p3 = SubProcessRule.objects.filter(ontology=objectOntology)[num_of_rules].p3
        subproperty_p2=[i["name"] for i in getStructure(action="Property",ontology=objectOntology) if i["parent"]==p2]
        subproperty_p3=[i["name"] for i in getStructure(action="Property",ontology=objectOntology) if i["parent"]==p3]
        dict_of_subprop_p2={} #{A:[A1,A2,...]}
        for subprop in subproperty_p2: 
            list_of_ranges=list(Triplets.objects.filter(ontology=objectOntology, \
                object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subprop, \
                object_property= "http://www.w3.org/2000/01/rdf-schema#range").values_list('object_destanation', flat=True))    
            dict_of_subprop_p2[list(set(list(Triplets.objects.filter(ontology=objectOntology, \
                object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subprop, \
                object_property= "http://www.w3.org/2000/01/rdf-schema#domain" \
                ).values_list("object_destanation", flat=True))))[0].split("#")[1]]=[i.split("#")[1] for i in list_of_ranges]
        ''' Получение структуры для следует_за [(k1,k2), (k2,k3)] '''
        dom_range_tuple_p3=[] #[(k1, k2), (k2,k3)]
        for el in subproperty_p3:
            dom = Triplets.objects.get(ontology=objectOntology, \
                object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + el, \
                object_property= "http://www.w3.org/2000/01/rdf-schema#domain").object_destanation.split("#")[1]
            ran = Triplets.objects.get(ontology=objectOntology, \
                object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + el, \
                object_property= "http://www.w3.org/2000/01/rdf-schema#range").object_destanation.split("#")[1]
            dom_range_tuple_p3.append((dom, ran)) #[(k1, k2), (k2,k3)]
        ''' Выстраивание в правильном порядке '''
        for items in dict_of_subprop_p2.items():
            first_element="" #Первый элемент в последовательности состоит из
            for it in items[1]:
                first = False
                second=False
                for j in dom_range_tuple_p3:
                    if (it)==j[0]: first = True
                    if (it)==j[1]: second = True
                if (first==True and second==False): #Тогда это первый элемент
                    first_element=it
                    break
            ret=[]
            ret.append(first_element)
            while True:
                flag = False
                for i in dom_range_tuple_p3:
                    if(i[0]==ret[-1]):
                        ret.append(i[1])
                        flag = True
                        break
                if (flag==False):
                    break
            dict_of_subprop_p2[items[0]]=ret
        '''  Поиск корневого элемента в [A:[a1...]] отсутствует в range у всех свойств состоит из '''
        list_first_concept=[] #Корневой концепт
        for it_sub_p2 in dict_of_subprop_p2.keys():
            flag=True
            for i in dict_of_subprop_p2.items():
                for j in i[1]:
                    if (it_sub_p2 == j):
                        flag=False
            if(flag):
                list_first_concept.append(it_sub_p2)
                
        ''' Выявление подпроцессов '''
        for first_concept in list_first_concept:
            list_process = []# Начальные список
            list_process.extend(dict_of_subprop_p2[first_concept])
            dict_of_subprop_p2.pop(first_concept)
            while True:
                flag = False
                for it_proc in list_process:
                    if it_proc in dict_of_subprop_p2.keys():
                        flag=True

                        buf=dict_of_subprop_p2[it_proc]
                        dict_of_subprop_p2.pop(it_proc)
                        ''' Добавление связи '''
                        index = list_process.index(it_proc)
                        if(len(list_process)>index+2):
                            addSubProperty(mail=settings.ROBOT_MAIL, ontology = ontology, propertyname = p2, subpropertyname = buf[-1]+"_"+p2+"_"+list_process[index+1])
                            addRelations(mail=settings.ROBOT_MAIL, ontology=ontology, first_object = buf[-1], second_object=list_process[index+1], \
                                property_object=buf[-1]+"_"+p2+"_"+list_process[index+1])

                        list_process.remove(it_proc)
                        list_process[index:index]=buf
                        break
                if (flag==False):
                    break
            ''' Добавление связи между A и концептами процесса '''
            for i in list_process:
                addSubProperty(mail=settings.ROBOT_MAIL, ontology= ontology, propertyname=p2, subpropertyname=first_concept+"_"+p2+"_"+i)
                if(addRelations(mail = settings.ROBOT_MAIL, ontology = ontology, first_object=first_concept, property_object=first_concept+"_"+p2+"_"+i, second_object=i)):
                    list_of_ret.append([first_concept, first_concept+"_"+p2+"_"+i, i])
                else:
                    pass
    return {"result":True, "list_of_result":list_of_ret}
def deleteSubProcessRule(pk):
    SubProcessRule.objects.get(pk=int(pk)).delete();
    return True
            
#####################################################################################################################
###########################################     РЕДАКТОР ОНТОЛОГИЙ      #############################################
#####################################################################################################################   

def addClass(mail, ontology, classname,subclassname, color = "#FAFF17"): #Добавление класса
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    '''Проверка наличия такого класса'''
    if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual").count()==0):
        new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class", color = color, data = datetime.now())
        new_triplets.save()
        return True
    else:
        return False
    

def addSubClass(mail, ontology, classname, subclassname, color = "#FAFF17"): #Добавление подкласса
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual").count()==0):
        new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, \
            object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, \
            object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", \
            object_destanation= "http://www.w3.org/2002/07/owl#Class", \
            color = color, data = datetime.now())
        new_triplets.save()
    else:
        return False
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subclassname, object_property= "http://www.w3.org/2000/01/rdf-schema#subClassOf", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + classname, color = color, data = datetime.now())
    new_triplets.save()
    return True        

def addProperty(mail, ontology, propertyname,subpropertyname, color = "#52CD6B"): #Добавление свойства
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual").count()==0):
    #if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0):
        new_triplets = Triplets(author=objectAuthor, \
            ontology=objectOntology, \
            object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, \
            object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", \
            object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty", color = color, data = datetime.now())
        new_triplets.save()
        return True
    else:
        return False

def addSubProperty(mail, ontology, propertyname, subpropertyname, color = "#52CD6B"): #Добавление подсвойства
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual").count()==0):
    #if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0):    
        new_triplets = Triplets(author=objectAuthor, \
            ontology=objectOntology, \
            object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, \
            object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", \
            object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty", color = color, data = datetime.now())
        new_triplets.save()
    else: 
        return False
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + subpropertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#subPropertyOf", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, data = datetime.now())
    new_triplets.save()
    return True

def addDomainProperty(mail, ontology, propertyname, domainname):
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#domain", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+domainname,).count()==0):
        new_triplets = Triplets(author=objectAuthor, \
            ontology=objectOntology, \
            object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, \
            object_property= "http://www.w3.org/2000/01/rdf-schema#domain", \
            object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+domainname, \
            data = datetime.now())
        new_triplets.save()
        return True
    else:
        return False
def addRangeProperty(mail, ontology, propertyname, rangename):
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#range", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+rangename).count()==0):
        new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, \
            object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, \
            object_property= "http://www.w3.org/2000/01/rdf-schema#range", \
            object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+rangename, \
            data = datetime.now())
        new_triplets.save()
        return True
    else:
        return False

def addIndividual(mail, ontology, individualname, classname):
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if (Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#ObjectProperty").count()==0 and Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual").count()==0):
    #if(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual").count()==0):
        new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#NamedIndividual", data = datetime.now())
        new_triplets.save()
    else:
        return False
    new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+ classname, data = datetime.now())
    new_triplets.save()
    return True

def addRestrictions(mail, ontology, first_concept, property_concept, second_concept):
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    new_restriction = RoleRestrictions(author=objectAuthor, ontology=objectOntology, first_concept="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_concept, first_role_second = "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+property_concept, second_concept="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_concept, data = datetime.now())
    new_restriction.save()

    return True

def addRelations(mail, ontology, first_object, property_object, second_object): #Добавление связи через domain range
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    flag=False
    if(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + property_object, object_property= "http://www.w3.org/2000/01/rdf-schema#domain", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_object).count()==0):
        new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + property_object, object_property= "http://www.w3.org/2000/01/rdf-schema#domain", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_object, data = datetime.now())
        new_triplets.save()
        flag=True

    if(Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + property_object, object_property= "http://www.w3.org/2000/01/rdf-schema#range", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_object).count()==0):
        new_triplets = Triplets(author=objectAuthor, ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + property_object, object_property= "http://www.w3.org/2000/01/rdf-schema#range", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_object, data = datetime.now())
        new_triplets.save()
        flag=True
    return flag



def addSimpleRule(mail, ontology, first_object, first_property, arrow_right, second_object, second_propery, second_arrow, third_object, third_property, third_arrow):
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if(SimpleRulesForOntology.objects.filter(ontology=objectOntology, first_concept = first_object, first_role_second = first_property, right_direction_first_role_first = (lambda x: True if x == 'True' else False)(arrow_right), second_concept = second_object, second_role_third = second_propery, right_direction_first_role_second = (lambda x : True if x == 'True' else False)(second_arrow), third_concept = third_object, transitive_node= third_property, right_direction_transitive_node = (lambda x : True if x == 'True' else False)(third_arrow)).count()==0):
        new_rule = SimpleRulesForOntology(author=objectAuthor, ontology=objectOntology, \
            first_concept = first_object, first_role_second = first_property, \
            right_direction_first_role_first = (lambda x: True if x == 'True' else False)(arrow_right), \
            second_concept = second_object, second_role_third = second_propery, \
            right_direction_first_role_second = (lambda x : True if x == 'True' else False)(second_arrow), third_concept = third_object, transitive_node= third_property, right_direction_transitive_node = (lambda x : True if x == 'True' else False)(third_arrow), data = datetime.now())
        new_rule.save()
        return True
    else:
        False

def addNewConceptRule(mail, ontology, k1, k3, p2): #Добавление правила с добавлением нового концепта
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if(NewConceptRule.objects.filter(ontology=objectOntology, k1=k1,k3=k3, p2=p2).count()==0):
        new_rule = NewConceptRule(author=objectAuthor, ontology=objectOntology, k1=k1,k3=k3, p2=p2, data = datetime.now())
        new_rule.save()
        return True
    else:
        return False

def addSubProcessRule(mail, ontology, p2, p3): #Добавление правила с выводом подпроцессов
    objectAuthor = get_object_or_404(MyUser, email = mail) #получение объекта автора
    objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    if(SubProcessRule.objects.filter(ontology=objectOntology, p2=p2, p3=p3,).count()==0):
        new_rule = SubProcessRule(author=objectAuthor, ontology=objectOntology, p2=p2, p3=p3, data = datetime.now())
        new_rule.save()
        return True
    else:
        return False

def deleteClassAndSubclasses(ontology, classname): #Удаление класса со всеми подклассами
    list_of_classes=getAllSubclassesOfClass(ontology=ontology, classname=classname)
    list_of_classes.append(classname)
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    for i in list_of_classes:
        Triplets.objects.filter(ontology=objectOntology, object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+i).delete()
        Triplets.objects.filter(ontology=objectOntology, object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+i).delete()
    return True
def deletePropertyAndSubproperty(ontology, propertyname):
    list_of_classes=getAllSubpropertyOfProperty(ontology=ontology, propertyname=propertyname)
    list_of_classes.append(propertyname)
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    for i in list_of_classes:
        Triplets.objects.filter(ontology=objectOntology, object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+i).delete()
        Triplets.objects.filter(ontology=objectOntology, object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+i).delete()
    return True

def deleteDomain(ontology, propertyname, domainname): #Удаление domain
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#domain", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+domainname).delete()
    return True 

def deleteRange(ontology, propertyname, rangename): #Удаление range
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#range", object_destanation= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+rangename).delete()
    return True 

def deleteIndividual(ontology,individualname): #Удаление экземпляра
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    Triplets.objects.filter(ontology=objectOntology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + individualname, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type").delete()
    return True

def deleteOntology(ontology):
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)
    Triplets.objects.filter(ontology=objectOntology).delete()
    SimpleRulesForOntology.objects.filter(ontology=objectOntology).delete()
    NewConceptRule.objects.filter(ontology=objectOntology).delete()
    SubProcessRule.objects.filter(ontology=objectOntology).delete()
    objectOntology.delete()
    return True
####################################################################################################################
#####################                                       GET                     ################################
####################################################################################################################

def getAllClasses(ontology): #Получение всех элементов, имеющих тип класс
    allClasses = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", \
        object_destanation="http://www.w3.org/2002/07/owl#Class").values_list('object_source', flat=True))
    if (len(allClasses)):
       allClasses = [i.split("#")[1] for i in allClasses]
    allClasses.insert(0,"#")
     #Добавление корневого элемента
    return allClasses

def getAllProperty(ontology): #Получение всех элементов, имеющих тип свойство
    allProperty = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation="http://www.w3.org/2002/07/owl#ObjectProperty").values_list('object_source', flat=True))
    if (len(allProperty)):
       allProperty = [i.split("#")[1] for i in allProperty]
    allProperty.insert(0,"#")
     #Добавление корневого элемента
    return allProperty

def getPropertyDomainRange(ontology, propertyname): #получение доменов и рэнжей свойства в виде [{domain:[], range:[]}]
    #objectOntology = get_object_or_404(CreatedOntology, name = ontology) #получение объекта онтологии
    domains = list(Triplets.objects.filter(ontology=ontology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#domain").values_list('object_destanation', flat=True))
    ranges = list(Triplets.objects.filter(ontology=ontology, object_source= "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + propertyname, object_property= "http://www.w3.org/2000/01/rdf-schema#range").values_list('object_destanation', flat=True))
    if (len(domains)):
       domains = [i.split("#")[1] for i in domains]
    if (len(ranges)):
       ranges = [i.split("#")[1] for i in ranges]
    return {"domain":domains, "range":ranges}
def getIndividualOfClass(classname, ontology): #получаем все экземпляры определенного класса
    allIndividual = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation='http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#'+classname).values_list('object_source', flat=True)) 
    if (len(allIndividual)):
       allIndividual = [i.split("#")[1] for i in allIndividual]
    return allIndividual

def getAllIndividuals(ontology): #получение всех экземпляров, вообще всех
    allIndividual = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation="http://www.w3.org/2002/07/owl#NamedIndividual").values_list('object_source', flat=True)) 
    if (len(allIndividual)):
       allIndividual = [i.split("#")[1] for i in allIndividual]
    return allIndividual

def getConcepts(ontology): #Получение всех концептов
    allClasses = getAllClasses(ontology = ontology) #Получение списка всех элементов, имеющих тип класс
    allSubClasses = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf").values_list('object_source', flat=True))
    allSubClasses = [i.split('#')[1] for i in allSubClasses]
    principals = list(set(allClasses).symmetric_difference(allSubClasses)) #Список принципов
    principals.remove('#') #Удаление корневого элемента
    return principals
def getPropertyConcepts(ontology): #Получение суперсвойств
    allClasses = getAllProperty(ontology = ontology) #Получение списка всех элементов, имеющих тип класс
    allSubClasses = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/2000/01/rdf-schema#subPropertyOf").values_list('object_source', flat=True))
    allSubClasses = [i.split('#')[1] for i in allSubClasses]
    principals = list(set(allClasses).symmetric_difference(allSubClasses)) #Список принципов
    principals.remove('#') #Удаление корневого элемента
    return principals
def getRestriction(ontology):
    restrictions = list(RoleRestrictions.objects.filter(ontology = ontology).values_list("first_concept","first_role_second", "second_concept"))
    buf = []
    for i in restrictions:
        buf.append([i[0].split('#')[1], i[1].split('#')[1], i[2].split('#')[1]])
    restrictions = buf
    print(restrictions)
    return restrictions

def getConceptOfObject(object, ontology): #получение суперкласса класса
    
    class_level_up = Triplets.objects.filter(ontology = ontology, object_source=object, object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf")
    if (len(class_level_up)!=0):
        return getConceptOfObject(class_level_up[0].object_destanation, ontology = ontology)
    else:
        return object

def getSuperPropertyOfObject(object, ontology): #получение суперкласса свойства
    
    class_level_up = Triplets.objects.filter(ontology = ontology, object_source=object, object_property="http://www.w3.org/2000/01/rdf-schema#subPropertyOf")
    if (len(class_level_up)!=0):
        return getSuperPropertyOfObject(class_level_up[0].object_destanation, ontology = ontology)
    else:
        return object

def recurseForgetAllSubclassesOfClass(ontology,classname,ret): #Рекурсивная функция для поиска всего дерева подклассов
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)    
    list_of_subclasses=[i["name"] for i in getStructure(action="Class", ontology=objectOntology) if i["parent"]==classname]
    if (len(list_of_subclasses)!=0):
        ret.extend(list_of_subclasses)
        for i in list_of_subclasses:
            ret.extend(recurseForgetAllSubclassesOfClass(ontology,i,ret))
        ret=list(set(ret))
        return ret
    else:
        ret=list(set(ret))
        return ret

def recurseForgetAllSubpropertyOfProperty(ontology,propertyname,ret): #Рекурсивная функция для поиска всего дерева подсвойств
    objectOntology = get_object_or_404(CreatedOntology, name = ontology)    
    list_of_subclasses=[i["name"] for i in getStructure(action="Property", ontology=objectOntology) if i["parent"]==propertyname]
    if (len(list_of_subclasses)!=0):
        ret.extend(list_of_subclasses)
        for i in list_of_subclasses:
            ret.extend(recurseForgetAllSubclassesOfClass(ontology,i,ret))
        ret=list(set(ret))
        return ret
    else:
        ret=list(set(ret))
        return ret

def getAllSubclassesOfClass(ontology, classname): #Получение всех подклассов класса в глубину в виде [...]
    list_of_subclasses=recurseForgetAllSubclassesOfClass(ontology=ontology, classname=classname, ret=[])
    return list_of_subclasses

def getAllSubpropertyOfProperty(ontology, propertyname):
    list_of_subclasses=recurseForgetAllSubpropertyOfProperty(ontology=ontology, propertyname=propertyname, ret=[])
    return list_of_subclasses

def getRelationRestriction(first_object, first_id, second_object, second_id, ontology): # получение возможных связей с наложенными ограничениями (фильтрация связей)
    if (first_id.find('class')!=-1): 
        if(second_id.find('class')!=-1): #класс класс
            first_concept = getConceptOfObject("http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_object, ontology = ontology,)
            second_concept = getConceptOfObject("http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_object, ontology = ontology,)
            return list(RoleRestrictions.objects.filter(first_concept = first_concept, second_concept= second_concept).values_list("first_role_second", flat=True))
        else:                             #класс экз
            first_concept = getConceptOfObject("http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_object, ontology = ontology,)
            second_concept = getConceptOfObject(Triplets.objects.get(Q(ontology = ontology), Q(object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_object), Q(object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), ~Q(object_destanation__contains="http://www.w3.org/2002/07/owl#NamedIndividual")).object_destanation)
            return list(RoleRestrictions.objects.filter(ontology = ontology, first_concept = first_concept, second_concept= second_concept).values_list("first_role_second", flat=True))
    else:
        if (second_id.find('individual')): # экз экз
            first_concept = getConceptOfObject(Triplets.objects.get(Q(object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_object), Q(object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), ~Q(object_destanation__contains="http://www.w3.org/2002/07/owl#NamedIndividual")).object_destanation)
            second_concept = getConceptOfObject(Triplets.objects.get(Q(object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_object), Q(object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), ~Q(object_destanation__contains="http://www.w3.org/2002/07/owl#NamedIndividual")).object_destanation)
            return list(RoleRestrictions.objects.filter(ontology = ontology, first_concept = first_concept, second_concept= second_concept).values_list("first_role_second", flat=True))
        else:                               # экз класс
            first_concept = getConceptOfObject(Triplets.objects.get(Q(ontology = ontology), Q(object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+first_object), Q(object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), ~Q(object_destanation__contains="http://www.w3.org/2002/07/owl#NamedIndividual")).object_destanation)
            second_concept = getConceptOfObject("http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+second_object, "Class", ontology = ontology)
            return list(RoleRestrictions.objects.filter(ontology = ontology, first_concept = first_concept, second_concept= second_concept).values_list("first_role_second", flat=True))

def getAllRelations(ontology): #список связей [(object1, type1, relation, object2,type2)]
    ret = []
    for prop in getAllProperty(ontology=ontology): #получение всех свойств 
        domain_of_prop = list(Triplets.objects.filter(ontology = ontology, object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+prop, object_property='http://www.w3.org/2000/01/rdf-schema#domain').values_list("object_destanation","author__email"))
        range_of_prop = list(Triplets.objects.filter(ontology = ontology, object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+prop, object_property='http://www.w3.org/2000/01/rdf-schema#range').values_list("object_destanation","author__email")) 
        if (len(domain_of_prop)!=0 and len(range_of_prop)!=0 ):
            for r in range_of_prop:
                for d in domain_of_prop:
                    try: 
                        auth = r[1]
                    
                        r_type = Triplets.objects.get(Q(ontology = ontology),Q(object_source=r[0]),Q(object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), Q(object_destanation__contains="Class") | Q(object_destanation__contains="IndividualName")).object_source
                        d_type = Triplets.objects.get(Q(ontology = ontology), Q(object_source=d[0]),Q(object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type'), Q(object_destanation__contains="Class") | Q(object_destanation__contains="IndividualName")).object_destanation
                        
                        if (r_type.find("Class")!=-1):
                            r_type="class"
                        else:
                            r_type="individual"
                        if (d_type.find("Class")!=-1):
                            d_type="class"
                        else:
                            d_type="individual"
                        ret.append((d[0].split('#')[1],d_type, prop, r[0].split('#')[1], r_type, auth))
                    except ObjectDoesNotExist:
                        pass
    return(ret)
def getAllSimpleRules(ontology):
    simpleRules = list(SimpleRulesForOntology.objects.filter(ontology = ontology).values_list("author__email", "author__avatar", 'first_concept', 'first_role_second', 'right_direction_first_role_first', 'second_concept', 'second_role_third', 'right_direction_first_role_second', 'third_concept', 'transitive_node', 'right_direction_transitive_node', 'data','pk'))
    return simpleRules
def getAllNewConceptRules(ontology):
    newConcptRules = list(NewConceptRule.objects.filter(ontology = ontology).values_list("author__email", "author__avatar", 'k1', 'k3', 'p2', 'data','pk'))
    return newConcptRules
def getAllSubProcessRules(ontology):
    newConcptRules = list(SubProcessRule.objects.filter(ontology = ontology).values_list("author__email", "author__avatar", 'p2', 'p3', 'data','pk'))
    return newConcptRules

def getColorObject(ontology, name, type):
    if (type == "Class"):
        return Triplets.objects.get(ontology = ontology, object_source = "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + name, object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type', object_destanation = "http://www.w3.org/2002/07/owl#Class").color
    elif (type == "Property"):
        return Triplets.objects.get(ontology = ontology, object_source = "http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#" + name, object_property='http://www.w3.org/1999/02/22-rdf-syntax-ns#type', object_destanation = "http://www.w3.org/2002/07/owl#ObjectProperty").color
    else: return "#ffffff"



def recurse_get_tree(iter_old_list, new_list, color):
    for i in new_list:
        flag=False
        if(iter_old_list["parent"]==i["text"]):
            if("nodes" in i):
                i["nodes"].append({"text": iter_old_list["name"], "backColor": color})
                flag=True
                break
            else:
                i["nodes"]=[{"text":iter_old_list["name"], "backColor": color}]
                flag=True
                break
    if(flag==True):
        return True, new_list
    else:
        ps=[]
        for index, value in enumerate(new_list):
            if("nodes" in value):
                ps.append(index)
        if(len(ps)!=0):
            for i in ps:
                b, new_list1=recurse_get_tree(iter_old_list, new_list[i]["nodes"], color)
                if(b==True):
                    return (True, new_list)
    return False, new_list
def getStructure(action, ontology):
    if(action == "Class"):
        principals = getConcepts(ontology)

        treeClassList = [] #Возвращаемое дерево классов в виде list
        
        for i in principals: #Добавление принциполов в список словарей
            treeClassList.append({'name': i, 'parent':'#'})
        
        allSubClassesAndParent = list(Triplets.objects.filter(ontology = ontology,object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf").values_list('object_source','object_destanation')) #Список кортежей (класс, родительский_класс)

        for i in allSubClassesAndParent: #Добавление классов и родителей
            treeClassList.append({'name': i[0].split("#")[1], 'parent':i[1].split("#")[1]})
        new_tree=[]
        ps=[]
        for val in treeClassList:
            if val["parent"]=="#":
                new_tree.append({"text":val["name"], "backColor":getColorObject(ontology, val["name"], "Class") })
                ps.append(val)
        for i in ps: #Удаление корневых элементов
            treeClassList.remove(i)
        

        return treeClassList
    if(action=="Class_new_struct"):
        principals = getConcepts(ontology)
        principals.sort()
        treeClassList = [] #Возвращаемое дерево классов в виде list
        for i in principals: #Добавление принциполов в список словарей
            treeClassList.append({'name': i, 'parent':'#'})
        allSubClassesAndParent = list(Triplets.objects.filter(ontology = ontology,object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf").values_list('object_source','object_destanation')) #Список кортежей (класс, родительский_класс)
        for i in allSubClassesAndParent: #Добавление классов и родителей
            treeClassList.append({'name': i[0].split("#")[1], 'parent':i[1].split("#")[1]})
        
        new_tree=[]
        ps=[]
        for val in treeClassList:
            if val["parent"]=="#":
                new_tree.append({"text":val["name"], "backColor":getColorObject(ontology, val["name"], "Class")})
                ps.append(val)
        for i in ps: #Удаление корневых элементов
            treeClassList.remove(i)
        old_list=treeClassList
        new_list=new_tree
        while(len(old_list)>0):
            flag = False
            rem=None
            for iter_old_list in old_list:
                b, new_list = recurse_get_tree(iter_old_list, new_list, getColorObject(ontology, iter_old_list["name"], "Class"))
                if(b==True):
                    flag = True
                    rem = iter_old_list
                    break
            if(flag==True):
                old_list.remove(rem)
        return [{"text":"#", "nodes":new_list}]
        

    if(action == "Property"):
        allProperty = getAllProperty(ontology) #Получение списка всех элементов, имеющих тип свойство
        allSubProperty = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/2000/01/rdf-schema#subPropertyOf").values_list('object_source', flat=True))
        allSubProperty = [i.split('#')[1] for i in allSubProperty]
        principals = list(set(allProperty).symmetric_difference(allSubProperty)) #Список принципов
        principals.remove('#') #Удаление корневого элемента

        treePropertyList = [] #Возвращаемое дерево свойств в виде list
        
        for i in principals: #Добавление принциполов в список словарей
            treePropertyList.append({'name': i, 'parent':'#'})
        
        allSubPropertyAndParent = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/2000/01/rdf-schema#subPropertyOf").values_list('object_source','object_destanation')) #Список кортежей (класс, родительский_класс)

        for i in allSubPropertyAndParent: #Добавление свойств и родителей
            treePropertyList.append({'name': i[0].split("#")[1], 'parent':i[1].split("#")[1]})
        
        return treePropertyList
    
    if(action=="Property_new_struct"):
        allProperty = getAllProperty(ontology) #Получение списка всех элементов, имеющих тип свойство
        allSubProperty = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/2000/01/rdf-schema#subPropertyOf").values_list('object_source', flat=True))
        allSubProperty = [i.split('#')[1] for i in allSubProperty]
        principals = list(set(allProperty).symmetric_difference(allSubProperty)) #Список принципов
        principals.remove('#') #Удаление корневого элемента

        principals.sort()
        treeClassList = [] #Возвращаемое дерево классов в виде list
        for i in principals: #Добавление принциполов в список словарей
            treeClassList.append({'name': i, 'parent':'#'})
        allSubClassesAndParent = list(Triplets.objects.filter(ontology = ontology,object_property="http://www.w3.org/2000/01/rdf-schema#subPropertyOf").values_list('object_source','object_destanation')) #Список кортежей (класс, родительский_класс)
        for i in allSubClassesAndParent: #Добавление классов и родителей
            treeClassList.append({'name': i[0].split("#")[1], 'parent':i[1].split("#")[1]})
        
        new_tree=[]
        ps=[]
        for val in treeClassList:
            if val["parent"]=="#":
                new_tree.append({"text":val["name"], "backColor":getColorObject(ontology, val["name"], "Property")})
                ps.append(val)
        for i in ps: #Удаление корневых элементов
            treeClassList.remove(i)
        old_list=treeClassList
        new_list=new_tree
        while(len(old_list)>0):
            flag = False
            rem=None
            for iter_old_list in old_list:
                b, new_list = recurse_get_tree(iter_old_list, new_list, getColorObject(ontology, iter_old_list["name"], "Property"))
                if(b==True):
                    flag = True
                    rem = iter_old_list
                    break
            if(flag==True):
                old_list.remove(rem)
        return [{"text":"#", "nodes":new_list}]



####################################################################################################################################################
#####################################################                       POST                ####################################################
####################################################################################################################################################


@method_decorator(csrf_exempt, name='dispatch')
class PostHandler(View):   
    def post(self, request): #Здесь обрабатываются события добавления, удаления класса, добавления, удаления событися ...
        print(request.POST)
        if request.user.is_authenticated: #проверка аутентификации пользователя
            mail = request.user.email
            ontology_name = request.session['ontology_name']
            if(ontology_name!=''):
                ontology = CreatedOntology.objects.get(name=ontology_name) #получение объекта онтологии из сессии
            else:
                ontology = None #Если запрос пришел на суперправила
            ##########################          Работа с классами              ##########################################

            if(request.POST.get("action")=="addClass" and request.POST.get("classname") == "#"): # Если добавление класса
                if(addClass(mail = mail, ontology= ontology, classname = request.POST.get("classname"),subclassname=request.POST.get("subclassname"), color = request.POST.get("color"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)

            if(request.POST.get("action")=="addSubClass" and request.POST.get("classname") != "#"): #Если добавление подкласса
                if(addSubClass(mail = mail, ontology= ontology,classname = request.POST.get("classname"), subclassname=request.POST.get("subclassname"), color = request.POST.get("color"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)

            if(request.POST.get("action")=="deleteClass"): # Удаление класса и подклассов
                if(deleteClassAndSubclasses(ontology= ontology, classname = request.POST.get("classname"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)
            #########################          Работа со свойствами            ##########################################
            
            if(request.POST.get("action")=="addProperty" and request.POST.get("propertyname") == "#"): # Если добавление класса
                if(addProperty(mail = mail, ontology= ontology, propertyname = request.POST.get("propertyname"),subpropertyname=request.POST.get("subpropertyname"), color = request.POST.get("color"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)

            if(request.POST.get("action")=="addSubProperty" and request.POST.get("protpertyname") != "#"): #Если добавление подкласса
                if(addSubProperty(mail = mail, ontology= ontology,propertyname = request.POST.get("propertyname"), subpropertyname=request.POST.get("subpropertyname"), color = request.POST.get("color"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="deleteProperty"): # Удаление свойства и подсвойств
                if(deletePropertyAndSubproperty(ontology= ontology, propertyname = request.POST.get("propertyname"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)
            
            
            #########################          Работа с экземплярами            ##########################################

            if(request.POST.get("action")=="addIndividual"):
                if(addIndividual(mail = mail, ontology= ontology,individualname = request.POST.get("individualname"), classname=request.POST.get("classname"))):
                    
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="deleteIndividual"):
                if(deleteIndividual(ontology= ontology,individualname = request.POST.get("individualname"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            

            if(request.POST.get("action")=="addRestrictions"):
                if(addRestrictions(mail = mail, ontology= ontology, first_concept = request.POST.get("first_concept"), property_concept=request.POST.get("property_concept"), second_concept=request.POST.get("second_concept"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            
            #########################           Работа со связями               ############################################
            if(request.POST.get("action")=="addRelations"):
                if(addRelations(mail = mail, ontology= ontology, first_object = request.POST.get("first_object"), property_object=request.POST.get("property_object"), second_object=request.POST.get("second_object"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="addDomainProperty"):
                if(addDomainProperty(mail = mail, ontology= ontology, propertyname = request.POST.get("propertyname"), domainname=request.POST.get("domainname"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="addRangeProperty"):
                if(addRangeProperty(mail = mail, ontology= ontology, propertyname = request.POST.get("propertyname"), rangename=request.POST.get("rangename"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="deleteDomain"):
                if(deleteDomain(ontology= ontology, propertyname = request.POST.get("propertyname"), domainname=request.POST.get("domainname"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="deleteRange"):
                if(deleteRange(ontology= ontology, propertyname = request.POST.get("propertyname"), rangename=request.POST.get("rangename"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="DeleteOntology"):
                if(deleteOntology(ontology= ontology)):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            
            #############################################################################################################
            ########################        Работа с правилами                  #########################################
            #############################################################################################################

            if(request.POST.get("action")=="addRules"):
                if(addSimpleRule(mail = mail, ontology= ontology, first_object = request.POST.get("first_object"), first_property=request.POST.get("first_property"), arrow_right=request.POST.get("first_arrow_right_true"), second_object=request.POST.get("second_object"), second_propery=request.POST.get("second_property"), second_arrow=request.POST.get("second_arrow_right_true"), third_object=request.POST.get("third_object"), third_property = request.POST.get("third_property"), third_arrow = request.POST.get("third_arrow_right_true"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="deleteSimpleRule"):
                if(deleteSimpleRule(ontology= ontology, pk = request.POST.get("pk"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)

            if(request.POST.get("action")=="applicationSimpleRule"):
                list_index_check_rule = request.POST.get("list_num_of_rules").split(',') #получение списка выбранных правил
                list_index_check_rule = [int(i) for i in list_index_check_rule]
                ret = applicationSimpleRule(mail = mail, ontology = ontology, list_num_of_rules= list_index_check_rule)
                return JsonResponse(ret, safe=False)
                
                #if(addSimpleRule(mail = mail, ontology= request.POST.get("ontology"), first_object = request.POST.get("first_object"), first_property=request.POST.get("first_property"), arrow_right=request.POST.get("first_arrow_right_true"), second_object=request.POST.get("second_object"), second_propery=request.POST.get("second_property"), second_arrow=request.POST.get("second_arrow_right_true"), third_object=request.POST.get("third_object"), third_property = request.POST.get("third_property"), third_arrow = request.POST.get("third_arrow_right_true"))):
                #    HttpResponse(status=200)
                #else: 
                #    HttpResponse(status=404)
            
            if(request.POST.get("action")=="addNewConceptRule"):
                if(addNewConceptRule(mail = mail, ontology=ontology, k1 = request.POST.get("k1"), k3=request.POST.get("k3"), p2=request.POST.get("p2"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)
            if(request.POST.get("action")=="applicationNewConceptRuele"):
                list_index_check_rule = request.POST.get("list_num_of_rules").split(',') #получение списка выбранных правил
                list_index_check_rule = [int(i) for i in list_index_check_rule]
                ret = applicationNewConceptRule(mail = mail, ontology = ontology, list_num_of_rules= list_index_check_rule)
                return JsonResponse(ret, safe=False)

            if(request.POST.get("action")=="deleteNewConceptRule"):
                if(deleteNewConceptRule(pk = request.POST.get("pk"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            
            if(request.POST.get("action")=="addSubProcessRule"):
                if(addSubProcessRule(mail = mail, ontology=ontology, p2 = request.POST.get("p2"), p3=request.POST.get("p3"))):
                    return HttpResponse(status=200)
                else:
                    return HttpResponse(status=404)
            
            if(request.POST.get("action")=="applicationSubProcessRule"):
                list_index_check_rule = request.POST.get("list_num_of_rules").split(',') #получение списка выбранных правил
                list_index_check_rule = [int(i) for i in list_index_check_rule]
                ret = applicationSubProcessRule(mail = mail, ontology = ontology, list_num_of_rules= list_index_check_rule)
                return JsonResponse(ret, safe=False)
                #if(addSimpleRule(mail = mail, ontology= request.POST.get("ontology"), first_object = request.POST.get("first_object"), first_property=request.POST.get("first_property"), arrow_right=request.POST.get("first_arrow_right_true"), second_object=request.POST.get("second_object"), second_propery=request.POST.get("second_property"), second_arrow=request.POST.get("second_arrow_right_true"), third_object=request.POST.get("third_object"), third_property = request.POST.get("third_property"), third_arrow = request.POST.get("third_arrow_right_true"))):
                #    HttpResponse(status=200)
                #else: 
                #    HttpResponse(status=404)
                
            if(request.POST.get("action")=="deleteSubProcessRule"):
                if(deleteSubProcessRule(pk = request.POST.get("pk"))):
                    return HttpResponse(status=200)
                else: 
                    return HttpResponse(status=404)
            
        return HttpResponse("hello")




####################################################################################################################################################
#####################################################                       GET                ####################################################
####################################################################################################################################################



class GetStructureOntology(View):
    def get(self, request):
        ontology_name = request.session['ontology_name']
        if(ontology_name!=''):
            ontology = CreatedOntology.objects.get(name=ontology_name) #получение объекта онтологии из сессии
        else:
            ontology = None #Если запрос пришел на суперправила

        if(request.GET.get("action")=="Class"):
            classTreeList = getStructure("Class", ontology=ontology) #Получение структуры классов в виде[{name:"", parent:""}]
            return JsonResponse(classTreeList, safe=False)
        if(request.GET.get("action")=="Class_new_struct"):
            classTreeList = getStructure("Class_new_struct", ontology=ontology) #Получение структуры классов в виде[{name:"", parent:""}]
            return JsonResponse(classTreeList, safe=False)
        if(request.GET.get("action")=="AllClass"):
            classTreeList = getStructure("AllClass", ontology=ontology) #Получение списка всех классов
            return JsonResponse(getAllClasses, safe=False)
        if(request.GET.get("action")=="ListOfAllClass"):
            classTreeList = sorted(getAllClasses(ontology=ontology)) #Получение списка всех классов
            return JsonResponse(classTreeList, safe=False)


        if(request.GET.get("action")=="Property"):
            propertyTreeList = getStructure("Property", ontology=ontology) #Получение свойств классов в виде[{name:"", parent:""}]
            return JsonResponse(propertyTreeList, safe=False)
        if(request.GET.get("action")=="Property_new_struct"):
            classTreeList = getStructure("Property_new_struct", ontology=ontology) #Получение структуры классов в нормальном виде
            return JsonResponse(classTreeList, safe=False)
        if(request.GET.get("action")=="PropertyDomainRange"): #Получение доменов конкретного свойства
            classTreeList = getPropertyDomainRange(ontology=ontology, propertyname=request.GET.get("propertyname")) #Получение списка доменов и ренджей в виде [{domains:[], ranges:[]}]
            return JsonResponse(classTreeList, safe=False)
        if(request.GET.get("action")=="AllProperty"): #Получение доменов конкретного свойства
            classTreeList = sorted(getAllProperty(ontology=ontology)) #Получение списка доменов и ренджей в виде [{domains:[], ranges:[]}]
            return JsonResponse(classTreeList, safe=False)
        if(request.GET.get("action")=="PropertyConcepts"): #Получение суперсвойств
            classTreeList = sorted(getPropertyConcepts(ontology=ontology)) #Получение списка доменов и ренджей в виде [{domains:[], ranges:[]}]
            return JsonResponse(classTreeList, safe=False)
        
        if(request.GET.get("action")=="Individual"):
            IndividualList = getIndividualOfClass(classname = request.GET.get("classname"), ontology=ontology)
            return JsonResponse(IndividualList, safe=False)
        if(request.GET.get("action")=="AllIndividual"): #Получение всех экземпляров
            IndividualList = sorted(getAllIndividuals(ontology=ontology))
            return JsonResponse(IndividualList, safe=False)
        
        if(request.GET.get("action")=="Concept"): #Поулчение классов верхнего уровня
            ConceptList = getConcepts(ontology=ontology)
            return JsonResponse(sorted(ConceptList), safe=False)
        if(request.GET.get("action")=="AllRestriction"): #Поулчение списка ограничений
            RestrictionList = getRestriction(ontology=ontology)
            return JsonResponse(RestrictionList, safe=False)

        if(request.GET.get("action")=="GetRelationRestriction"): #Поулчение списка доступных связей
            Relations = getRelationRestriction(request.GET.get("first_object"), request.GET.get("first_id"), request.GET.get("second_object"), request.GET.get("second_id"), ontology=ontology)
            return JsonResponse(Relations, safe=False)

        if(request.GET.get("action")=="AllRelations"): #Получение всех связей
            RelationList = getAllRelations(ontology=ontology)
            return JsonResponse(RelationList, safe=False)
        
        if(request.GET.get("action")=="PropertyDomainRange"): #Получение [[],[]] ограниченией для простых правил
            RelationList = getAllRelations(ontology=ontology)
            return JsonResponse(RelationList, safe=False)
        
        if(request.GET.get("action")=="AllSimpleRules"): #Получение всех простых правил
            SimpleRuleList = getAllSimpleRules(ontology=ontology)
            return JsonResponse(SimpleRuleList, safe=False)
        if(request.GET.get("action")=="AllNewConceptRules"): #Получение всех простых правил
            SimpleRuleList = getAllNewConceptRules(ontology=ontology)
            return JsonResponse(SimpleRuleList, safe=False)
        if(request.GET.get("action")=="AllSubProcessRules"): #Получение всех простых правил
            SimpleRuleList = getAllSubProcessRules(ontology=ontology)
            return JsonResponse(SimpleRuleList, safe=False)
        if(request.GET.get("action")=="getColorObject"): #Получение всех простых правил
            color = getColorObject(ontology=ontology, name = request.GET.get("name"), type=request.GET.get("type"))
            return JsonResponse(color, safe=False)
        
##########################################################
#       OWL_WEB                ####################################################
###############

class DB_to_OWL(View):
    def get(self, request):
        ontology = CreatedOntology.objects.get(name=request.session['ontology_name'])
        allClasses = getAllClasses(ontology = ontology) #Получение списка всех элементов, имеющих тип класс
        '''Получение кортежей (подкласс, класс)'''
        allSubClasses = list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/2000/01/rdf-schema#subClassOf").values_list('object_source', 'object_destanation','author__email'))
        allSubClasses = [(i[0].split('#')[1],i[1].split('#')[1], i[2]) for i in allSubClasses]
        '''Получение суперклассов'''
        principals = list(set(allClasses).symmetric_difference([i[0] for i in allSubClasses])) #Список принципов
        principals.remove('#')
        principals = [(i, Triplets.objects.filter(ontology=ontology, object_source="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+i, object_property= "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation= "http://www.w3.org/2002/07/owl#Class").values_list('author__email', flat=True)[0]) for i in principals]
        '''Получение кортежей (экземпляр, класс)'''
        indivs=[]
        for i in allClasses:
            list_of_indiv=list(Triplets.objects.filter(ontology = ontology, object_property="http://www.w3.org/1999/02/22-rdf-syntax-ns#type", object_destanation="http://www.semanticweb.org/valsosuke/ontologies/2020/9/untitled-ontology-18#"+i).values_list('object_source', 'author__email'))
            if list_of_indiv:
                for j in list_of_indiv:
                    indivs.append((j[0].split("#")[1],i, j[1]))
        '''Формирование json для записи'''
        json_a_json={}
        list_of_nodes=[]#создание списка вершин
        #добавление корневого элемента Thin
        list_of_nodes.append({\
            'id':'Thin', \
            'label':'Thin', \
            'x':random.randrange(0,100), \
            'y':random.randrange(0,50), \
            'size':3, \
            'color': '#ffcc00' \
        })
        for i in principals:
            list_of_nodes.append({\
                'borderColor':'#ffcc00', \
                'id':i[0], \
                'label':i[0], \
                'type':'circle', \
                'image': {'clip':0.85, 'scale': 1.3, 'url': MyUser.objects.get(email=i[1]).avatar.url}, \
                'x':random.randrange(0,100), \
                'y':random.randrange(0,50), \
                'size':3, \
                'color': getColorObject(ontology, i[0], "Class"), \
                'borderColor': getColorObject(ontology, i[0], "Class"),\
                
            })
        for i in allSubClasses:
            list_of_nodes.append({\
                'id':i[0], \
                'label':i[0], \
                'type':'circle', \
                'image': {'clip':0.85, 'scale': 1.3, 'url': MyUser.objects.get(email=i[2]).avatar.url}, \
                'x':random.randrange(0,100), \
                'y':random.randrange(0,50), \
                'size':2, \
                'color': getColorObject(ontology, i[0], "Class"), \
                'borderColor': getColorObject(ontology, i[0], "Class"), \
            })
        for i in indivs:
            list_of_nodes.append({'id':i[0], \
                'label':i[0], \
                'type':'circle', \
                'image': {'clip':0.85, 'scale': 1.3, 'url': MyUser.objects.get(email=i[2]).avatar.url}, \
                'x':random.randrange(0,100), \
                'y':random.randrange(0,50), \
                'size':1, \
                'color': '#660066'\
            })
        list_of_edges=[] #список ребер
        for i in principals:
            list_of_edges.append({\
                'id':"Thin"+i[0], \
                "label": "subClassOf", \
                'source':i[0], \
                'target':'Thin', \
                "type": 'curvedArrow', \
                "size":10, \
                "color": "#ffff00", \
            })
        for i in allSubClasses:
            list_of_edges.append({\
                'id':i[0]+i[1], \
                "label": "subClassOf", \
                'source':i[0], \
                'target':i[1], \
                "type": 'curvedArrow', \
                "size":10, \
                "color": "#ffff00", \
            })
        for i in indivs:
            list_of_edges.append({ \
                'id':i[0]+i[1], \
                'label': 'individualOf', \
                'source':i[0], \
                'target':i[1], \
                "type": "arrow", \
                "size":10, \
                "color": "#660066"
            })
        aaa = list(set(getAllRelations(ontology)))
        
        for i in aaa:
            if(i[5]!=settings.ROBOT_MAIL):
                list_of_edges.append({ \
                    'id':i[0]+'_'+i[2]+'_'+i[3], \
                    'label': i[2], \
                    'source':i[0], \
                    'target':i[3], \
                    "type": 'arrow', \
                    "size":10, \
                    "color": getColorObject(ontology, i[2], "Property"), \
                })
            else:
                list_of_edges.append({ \
                    'id':i[0]+'_'+i[2]+'_'+i[3], \
                    'label': i[2], \
                    'source':i[0], \
                    'target':i[3], \
                    "type": 'dotted', \
                    "size":10, \
                    "color": getColorObject(ontology, i[2], "Property"), \
                })
        
        json_a_json['nodes']=list_of_nodes
        json_a_json['edges']=list_of_edges
        '''Раота с файлом a.json'''
        return JsonResponse({'result':True, 'data':json_a_json})

class SuperRule(View):
    def get(self, request):
        request.session['ontology_name']=''
        return render(request, "app1/superrule.html")

