



function GetSyncRequest(url) //отправка синхронного или ассинхронного GET запроса
{
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, false);
  try {
    xhr.send();
    if (xhr.status != 200) {
      alert(`Ошибка ${xhr.status}: ${xhr.statusText}`);
    }
  } catch(err) { // для отлова ошибок используем конструкцию try...catch вместо onerror
  alert("Запрос не удался");
  }
  return xhr;
}

function PostSyncRequest(url, body) //отправка синхронного или ассинхронного POST запроса
{
  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = function() {
      }
  xhr.send(body);
  return xhr;
}

function removeIndexes(arr, indexes){ //Удаление элементов по списку индексов из массива
  var newarray = [];
  for (var i=0; i<arr.length; i++)
  {
    var flag = true; //не входит
    for(var j =0; j<indexes.length; j++)
    {
      if(indexes[j]==i)
      {
        flag = false;
        break;
      }
    }
    if(flag==true)
    {
      newarray.push(arr[i])
    }
  }
  return newarray;  
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////                    ПОЛУЧЕНИЕ                       ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function loadStructureClass() //Получение структуры классов
{
    /*
    var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Class').response); //получение сруктуры классов из django
    
    //Теперь получаем стуртуру в виде [[name,parent], ...]
    var listOfStructure = [];
    
    for (var i=0; i<jsonStructClass.length; i++)
    {
      listOfStructure.push([jsonStructClass[i].name, jsonStructClass[i].parent] ) //Отделение префикса
    }
    //Получаем корневой ul
    var root_ul = document.getElementById("tree");
    var root_ul_individual = document.getElementById("treeClassIndividuals");
    
    root_ul_individual.innerHTML = "";
    root_ul.innerHTML="";

    var pos = [] //позиции, которые надо удалить
    var iter = 0;
    var tree_of_get=[];
    for (var i=0; i<listOfStructure.length; i++)
    {
     if(listOfStructure[i][1]=="#")
     {
      var li = document.createElement("li");
      var ul = document.createElement("ul");
      var individual_li = document.createElement("li");
      var individual_ul = document.createElement("ul");

      li.append(listOfStructure[i][0]);
      li.setAttribute('name', listOfStructure[i][0]);
      li.setAttribute('id', listOfStructure[i][0]);
      li.append(ul);
      individual_li.append(listOfStructure[i][0]);
      individual_li.setAttribute('name', "individual_"+listOfStructure[i][0]);
      individual_li.setAttribute('id', "individual_"+listOfStructure[i][0]);
      individual_li.append(individual_ul);

      tree_of_get.append({'text':listOfStructure[i][0], 'nodes':{}})

      root_ul.append(li);
      root_ul_individual.append(individual_li);
      pos.push(iter);
     }
     iter++; 
    }
    //Удаление корневых элементов из списка
    listOfStructure = removeIndexes(listOfStructure, pos); //Удаление элементов, которые входили в root
    
    while(listOfStructure.length>0)
    {
      iter = 0;
      pos = [];
      list_li = root_ul.getElementsByTagName("li") //получаем все li["name"], чтобы отследить, куда прикреплять, следующий элемент
      individual_list_li = root_ul_individual.getElementsByTagName("li")
      for (var i=0; i<listOfStructure.length; i++)
      {
        for (var j=0; j<list_li.length; j++)
        {
          if(listOfStructure[i][1] == list_li[j].getAttribute("name"))
          {
            li = document.createElement("li");
            li.append(listOfStructure[i][0]);
            li.setAttribute('name', listOfStructure[i][0]);
            li.setAttribute('id', listOfStructure[i][0]);

            individual_li = document.createElement("li");
            individual_li.append(listOfStructure[i][0]);
            individual_li.setAttribute('name', "individual_"+listOfStructure[i][0]);
            individual_li.setAttribute('id', "individual_"+listOfStructure[i][0]);

            var ul = document.createElement("ul");
            var individual_ul = document.createElement("ul");

            individual_li.append(individual_ul);
            li.append(ul);

            list_li[j].getElementsByTagName("ul")[0].append(li);
            individual_list_li[j].getElementsByTagName("ul")[0].append(individual_li);

            pos.push(iter);
          }
        }
        iter++;
      }
      listOfStructure = removeIndexes(listOfStructure, pos);
    }
    console.log(listOfStructure)
    appendSpan(root_ul);
    appendSpan(root_ul_individual);
    
    
    var c = document.getElementById("tree").getElementsByTagName("span");
    */

    var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Class_new_struct').response); //получение сруктуры классов из django
    $('#tree').treeview({data: jsonStructClass, borderColor: "black", levels:2, backColor: "yellow"});
    $('#tree').on('nodeSelected', function(event, data) {
      const name = data.text;
      const color = getParentObjectColor(name,"Class")
    })
    $('#treeClassIndividuals').treeview({data: jsonStructClass, borderColor:"black", levels:2, backColor: "yellow"});
    
    $('#treeClassIndividuals').on('nodeSelected', function(event, data) {
      console.log(event);
      displayIndividuals(data.text);
    });
}

function getParentObjectColor(name, type){
  if (type == "Class"){
    var color = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=getColorObject&name=' + name + '&type=Class').response);
    inpute_color_class.value = color;
  }
  if (type == "Property"){
    var color = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=getColorObject&name=' + name + '&type=Property').response);
    inpute_color_property.value = color;
  }
    
}

function loadStructureProperty() //Получение структуры совйств
{
    /*var jsonStructProperty = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Property').response); //получение сруктуры классов из django
    
    //Теперь получаем стуртуру в виде [[name,parent], ...]
    var listOfStructure = [];
    
    for (var i=0; i<jsonStructProperty.length; i++)
    {
      listOfStructure.push([jsonStructProperty[i].name, jsonStructProperty[i].parent] ) //Отделение префикса
    }
    //Получаем корневой ul
    var root_ul = document.getElementById("treeProperty");
    root_ul.innerHTML="";
    var pos = [] //позиции, которые надо удалить
    var iter = 0;
    for (var i=0; i<listOfStructure.length; i++)
    {
     if(listOfStructure[i][1]=="#")
     {
      var li = document.createElement("li");
      var ul = document.createElement("ul");
      li.append(listOfStructure[i][0]);
      li.setAttribute('name', listOfStructure[i][0]);
      li.setAttribute('id', listOfStructure[i][0]);
      li.append(ul);
      root_ul.append(li);
      pos.push(iter);
     }
     iter++; 
    }
    //Удаление корневых элементов из списка
    listOfStructure = removeIndexes(listOfStructure, pos); //Удаление элементов, которые входили в root
    while(listOfStructure.length>0)
    {
      iter = 0;
      pos = [];
      list_li = root_ul.getElementsByTagName("li") //получаем все li["name"], чтобы отследить, куда прикреплять, следующий элемент
      for (var i=0; i<listOfStructure.length; i++)
      {
        for (var j=0; j<list_li.length; j++)
        {
          if(listOfStructure[i][1] == list_li[j].getAttribute("name"))
          {
            li = document.createElement("li");
            li.append(listOfStructure[i][0]);
            li.setAttribute('name', listOfStructure[i][0]);
            li.setAttribute('id', listOfStructure[i][0]);
            var ul = document.createElement("ul");
            li.append(ul);
            list_li[j].getElementsByTagName("ul")[0].append(li);
            pos.push(iter);
          }
        }
        iter++;
      }
      listOfStructure = removeIndexes(listOfStructure, pos);
    }
    appendSpan(root_ul);
    
    var c = document.getElementById("treeProperty").getElementsByTagName("span");
    */
    var jsonStructProperty = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Property_new_struct').response);
    $('#treeProperty').treeview({data: jsonStructProperty, borderColor: "black", levels:2, backColor: "#67c255"});
    $('#treeProperty').on('nodeSelected', function(event, data) {
      const name = data.text;
      const color = getParentObjectColor(name,"Property")
    })
    $('#tree_property_for_rlation').treeview({data: jsonStructProperty, borderColor: "black", levels:2, backColor: "#67c255"});
    $('#tree_property_for_rlation').on('nodeSelected', function(event, data) {
      console.log(event);
      displayDomainAndRange(data.text);
    });
}

function displayIndividuals(event)
{

  /*if(event.target.parentNode.getAttribute("name")!=null | event.target.parentNode.getAttribute("name")!=undefined  )
  {*/
    //var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Individual&classname='+event.target.parentNode.getAttribute("name").split("_")[1]).response);
    list_individuals.innerHTML="";
    if (event!="")
    {
      var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Individual&classname='+event).response);
      if (jsonStructClass.length !=0)
      {
        list_individuals = document.getElementById("list_individuals");
        
        for(var i = 0; i<jsonStructClass.length; i++)
        {
          var a = document.createElement("a");
          a.setAttribute("id",jsonStructClass[i]);
          a.setAttribute("name",jsonStructClass[i])
          a.setAttribute("class", "list-group-item list-group-item-action");
          a.setAttribute("data-toggle","list");
          a.setAttribute("href","#list-home");
          a.setAttribute("role","tab");
          a.setAttribute("aria-controls","home");
          a.innerHTML=jsonStructClass[i];
          list_individuals.append(a);
        }
      }
    }
    
  //}
  /*if(event.target.parentNode.getAttribute("name")!=null | event.target.parentNode.getAttribute("name")!=undefined  )
  {

  }*/
  
}

function displayDomainAndRange(PropertyName){
  property_domain.innerHTML="";
  property_range.innerHTML="";
    if (PropertyName!="")
    {
      var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=PropertyDomainRange&propertyname='+PropertyName).response);
      if (jsonStructClass.length !=0)
      {
        for(var i=0; i<jsonStructClass["domain"].length; i++)
        {
          var a = document.createElement("a");
          a.setAttribute("id",jsonStructClass["domain"][i]);
          a.setAttribute("name",jsonStructClass["domain"][i])
          a.setAttribute("class", "list-group-item list-group-item-action");
          a.setAttribute("data-toggle","list");
          a.setAttribute("href","#list-home");
          a.setAttribute("role","tab");
          a.setAttribute("aria-controls","home");
          a.innerHTML=jsonStructClass["domain"][i];
          property_domain.append(a);
        }
        
        for(var i = 0; i<jsonStructClass["range"].length; i++)
        {
          var a = document.createElement("a");
          a.setAttribute("id",jsonStructClass["range"][i]);
          a.setAttribute("name",jsonStructClass["range"][i])
          a.setAttribute("class", "list-group-item list-group-item-action");
          a.setAttribute("data-toggle","list");
          a.setAttribute("href","#list-home");
          a.setAttribute("role","tab");
          a.setAttribute("aria-controls","home");
          a.innerHTML=jsonStructClass["range"][i];
          property_range.append(a);
        }
      }
    }
}

function getAllRestriction()
{
  
  var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllRestriction').response);
  return jsonStructClass;
}

function displayRestrictions() // отображение всех ограничений
{
  var a = getAllRestriction(); //получение списка словарей {first_concept, first_role_second, second_concept}
  var div = document.getElementById("allRestriction"); //получение div
  div.innerHTML="";
  for (var i = 0; i<a.length; i++)
  {
    var p = document.createElement("p"); //добавление параграфа
    p.innerHTML = a[i][0] + "     " + a[i][1] + "      " + a[i][2];
    div.append(p);
  }
}

function displayRelations()
{
  var a = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllRelations').response);
  var div = document.getElementById("allRelations"); //получение div
  div.innerHTML="";
  for (var i = 0; i<a.length; i++)
  {
    var p = document.createElement("p"); //добавление параграфа
    p.innerHTML = a[i][0] + "     " + a[i][2] + "      " + a[i][3];
    div.append(p);
  }
}



function deleteSimpleRule(){
    if(list_simple_rules.getElementsByClassName("active").length!=0){
      var pk = list_simple_rules.getElementsByClassName("active")[0].id;
      var body = 'action=' + "deleteSimpleRule" + '&pk=' + encodeURIComponent(pk);
      var resp = PostSyncRequest('/ontology/posthandler/', body);
  
      if (resp.status == "200")
      {
        displaySimpleRules();
        console.log("OK");
      }
      else
      {
        alert("Ошибка при удалении!");
      }
    }
    else{
      alert("Выберите правило для удаления!");
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////                    ДОБАВЛЕНИЕ                      ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function addClass(elem) //Добавление класса
{
  var list_selected_class = $('#tree').treeview('getSelected');
  var subclassName = document.getElementById("subClassName").value;
  const color = inpute_color_class.value;
  if (list_selected_class.length!=0 && subclassName!="")
  {
    var className = list_selected_class[0]["text"];
    if (className == "#"){
      
      var body = 'action=' + "addClass" + '&ontology=' + 'firstontology' + '&classname=' + encodeURIComponent(className) + '&subclassname=' + encodeURIComponent(subclassName) + '&color=' + color;
      
      
      var resp = PostSyncRequest('/ontology/posthandler/', body)
      if(resp.status == 200) //Проверка на ответа сервера по добавлению
      {
        loadStructureClass();
        reload_class_concept();
      }
      else
      {
        alert("Такой объект уже существует!");
      }
    }
    else
    {
      var body = 'action=' + "addSubClass" + '&ontology=' + 'firstontology' + '&classname=' + encodeURIComponent(className) + '&subclassname=' + encodeURIComponent(subclassName) + '&color=' + color;
      var resp = PostSyncRequest('/ontology/posthandler/', body)
      if(resp.status == 200) //Проверка на ответа сервера по добавлению
      {
        loadStructureClass();
        reload_class_concept();
      }
      else
      {
        alert("Такой подкласс уже существует!");
      }
    }
    
  }
  else{
    alert("Родительский класс не выбран!");
  }
}


function addProperty(elem) //Добавление свойства
{
  var list_selected_class = $('#treeProperty').treeview('getSelected');
  var subclassName = document.getElementById("subPropertyName").value;
  const color = inpute_color_property.value;
  
  if (list_selected_class.length!=0 && subclassName!="")
  {
    var className = list_selected_class[0]["text"];
    if (className == "#"){
      
      var body = 'action=' + "addProperty" + '&ontology=' + 'firstontology' + '&propertyname=' + encodeURIComponent(className) + '&subpropertyname=' + encodeURIComponent(subclassName) + '&color=' + color;
      
      
      var resp = PostSyncRequest('/ontology/posthandler/', body)
      if(resp.status == 200) //Проверка на ответа сервера по добавлению
      {
        loadStructureProperty();
        reload_property();
      }
      else
      {
        alert("Такое объект уже существует!");
      }
    }
    else
    {
      var body = 'action=' + "addSubProperty" + '&ontology=' + 'firstontology' + '&propertyname=' + encodeURIComponent(className) + '&subpropertyname=' + encodeURIComponent(subclassName) + '&color=' + color;
      var resp = PostSyncRequest('/ontology/posthandler/', body)
      if(resp.status == 200) //Проверка на ответа сервера по добавлению
      {
        loadStructureProperty();
        reload_property();
      }
      else
      {
        alert("Такое подсвойство уже существует!");
      }
    }
    
  }
  else{
    alert("Родительское свойство не выбрано!");
  }
}

function addDomain()
{
  var property_name = $('#tree_property_for_rlation').treeview('getSelected');
  if (property_name.length!=0)
  {
    property_name = property_name[0]['text'];
    var domain_name = list_class_and_individual_for_domain.value;
    if (domain_name!="")
    {
      var body = 'action=' + 'addDomainProperty' + '&propertyname=' + encodeURIComponent(property_name) + '&domainname=' + encodeURIComponent(domain_name);
        var resp = PostSyncRequest('/ontology/posthandler/', body)
        if(resp.status == 200) //Проверка на ответа сервера по добавлению
        {
          displayDomainAndRange(property_name);
        }
        else
        {
          alert("У этого отношения такой Domain уже существует!");
        }
        
    }
    else
    {
      alert("Выберите Domain!");
    }
  }
  else
  {
    alert("Выберите отношение!");
  }
}
function addRange()
{
  var property_name = $('#tree_property_for_rlation').treeview('getSelected');
  if (property_name.length!=0)
  {
    property_name = property_name[0]['text'];
    var domain_name = list_class_and_individual_for_range.value;
    if (domain_name!="")
    {
      var body = 'action=' + 'addRangeProperty' + '&propertyname=' + encodeURIComponent(property_name) + '&rangename=' + encodeURIComponent(domain_name);
        var resp = PostSyncRequest('/ontology/posthandler/', body)
        if(resp.status == 200) //Проверка на ответа сервера по добавлению
        {
          displayDomainAndRange(property_name);
        }
        else
        {
          alert("У этого отношения такой Domain уже существует!");
        }
        
    }
    else
    {
      alert("Выберите Range!");
    }
  }
  else
  {
    alert("Выберите отношение!");
  }
}

function addIndividual(elem) //Добавление экземпляра
{
  var individualClass = $('#treeClassIndividuals').treeview('getSelected');
  if (individualClass.length!=0)
  {
    individualClass = individualClass[0]["text"];
    var individualName = document.getElementById("individualName").value;
    if(individualName!="")
    {
      var body = 'action=' + "addIndividual" + '&ontology=' + 'firstontology' + '&individualname=' + encodeURIComponent(individualName) + '&classname=' + encodeURIComponent(individualClass);
      var resp = PostSyncRequest('/ontology/posthandler/', body)
      if (resp.status == 200)
      {
        displayIndividuals($('#treeClassIndividuals').treeview('getSelected')[0]["text"]);
        reload_class_concept();
      }
      else
      {
        alert("Такой объект уже существует!");
      }
    }
  } else{
    alert("Выберите сначало класс!");
  } 
}

function addRestrictions() //Добавление ограничения
{
  var first_concept = document.getElementById("first_concept").options[document.getElementById("first_concept").selectedIndex].text;
  var property_concept = document.getElementById("property_concept").options[document.getElementById("property_concept").selectedIndex].text;
  var second_concept = document.getElementById("second_concept").options[document.getElementById("second_concept").selectedIndex].text;
  
  var body = 'action=' + "addRestrictions" + '&ontology=' + 'firstontology' + '&first_concept=' + encodeURIComponent(first_concept) + '&property_concept=' + encodeURIComponent(property_concept)  + '&second_concept=' + encodeURIComponent(second_concept);
  var resp = PostSyncRequest('/ontology/posthandler/', body)
  if (resp.status == "200")
  {
    displayRestrictions();
    console.log("OK");
  }
  else
  {
    console.log("BAD BOY")
  }
  
}

function addRelations() //Добавление связей
{
  var first_object = document.getElementById("first_object").options[document.getElementById("first_object").selectedIndex].text;
  var select_relation_property_object = document.getElementById("select_relation_property_object").options[document.getElementById("select_relation_property_object").selectedIndex].text;
  var second_object = document.getElementById("second_object").options[document.getElementById("second_object").selectedIndex].text;

  var body = 'action=' + "addRelations" + '&ontology=' + 'firstontology' + '&first_object=' + encodeURIComponent(first_object) + '&property_object=' + encodeURIComponent(select_relation_property_object)  + '&second_object=' + encodeURIComponent(second_object);
  var resp = PostSyncRequest('/ontology/posthandler/', body)
  if (resp.status == "200")
  {
    
    console.log("OK");
  }
  else
  {
    console.log("BAD BOY")
  }
  
}

function addSimpleRuele()
{
  var first_object = simple_rules_object_first.value;

  var first_property = simple_rules_property_first_second.value;
  
  var first_arrow_right_true = raw_image1.dataset.right;

  var second_object = simple_rules_object_second.value;
  
  var second_property = simple_rules_property_second_third.value;
  var second_arrow_right_true = raw_image2.dataset.right;

  var third_object = simple_rules_object_third.value;

  var third_property = simple_rules_property_first_third.value;
  var third_arrow_right_true = raw_image3.dataset.right;
  if(first_object=="" || first_property=="" || second_object =="" || second_property=="" || third_object=="" || third_property==""){
    alert("Не все поля заполнены!");
    return 0;
  }

  var body = 'action=' + "addRules" + '&ontology=' + 'firstontology' + '&first_object=' + encodeURIComponent(first_object) + '&first_property=' + encodeURIComponent(first_property) + '&first_arrow_right_true=' + encodeURIComponent(first_arrow_right_true) + '&second_object=' + encodeURIComponent(second_object) + '&second_property=' + encodeURIComponent(second_property) + '&second_arrow_right_true=' + encodeURIComponent(second_arrow_right_true) + '&third_object=' + encodeURIComponent(third_object) + '&third_property=' + encodeURIComponent(third_property) + '&third_arrow_right_true=' + encodeURIComponent(third_arrow_right_true);
  var resp = PostSyncRequest('/ontology/posthandler/', body);

  if (resp.status == 200)
  {
    displaySimpleRules();
  }
  else
  {
    alert("Такое правило уже существует!");
  }

}

function addNewConceptRule()
{
  var k1 = setNewConceptRule_k1.value;
  var k3 = setNewConceptRule_k3.value;
  var p2 = setNewConceptRule_p2.value;
  if(k1=="" || k3=="" || p2 ==""){
    alert("Не все поля заполнены!");
    return 0;
  }
  var body = 'action=' + "addNewConceptRule" + '&k1=' + encodeURIComponent(k1) + '&k3=' + encodeURIComponent(k3) + '&p2=' + encodeURIComponent(p2);
  var resp = PostSyncRequest('/ontology/posthandler/', body);

  if (resp.status == 200)
  {
    displayNewConceptRule();
  }
  else
  {
    alert("Такое правило уже существует!");
  }
}

function addSubProcessRule()
{
  var p2 = setSubProcessRule_p2.value;
  var p3 = setSubProcessRule_p3.value;
  if(p2=="" || p3==""){
    alert("Не все поля заполнены!");
    return 0;
  }
  var body = 'action=' + "addSubProcessRule" + '&ontology=' + 'firstontology' + '&p2=' + encodeURIComponent(p2) + '&p3=' + encodeURIComponent(p3);
  var resp = PostSyncRequest('/ontology/posthandler/', body);

  if (resp.status == "200")
  {
    displaySubProcessRule();
  }
  else
  {
    alert("Такое правило уже существует!");
  }
}



function deleteClass(thisa)
{
  var list_selected_class = $('#tree').treeview('getSelected');
  if (list_selected_class.length!=0)
  {
    var classname=list_selected_class["0"]["text"];
    var body = 'action=' + "deleteClass" + '&ontology=' + 'firstontology' + '&classname=' + encodeURIComponent(classname);
    var resp = PostSyncRequest('/ontology/posthandler/', body);

    if (resp.status == "200")
    {
      loadStructureClass();
      reload_class_concept();
      console.log("OK");
    }
    else
    {
      alert("Ошибка при удалении!");
    }
  }
  else
  {
    alert("Выберите класс, который хотите удалить!");
  }
}

function deleteProperty(thisa)
{
  var list_selected_class = $('#treeProperty').treeview('getSelected');
  if (list_selected_class.length!=0)
  {
    var classname=list_selected_class["0"]["text"];
    var body = 'action=' + "deleteProperty" + '&propertyname=' + encodeURIComponent(classname);
    var resp = PostSyncRequest('/ontology/posthandler/', body);

    if (resp.status == "200")
    {
      loadStructureProperty();
      reload_property();
      console.log("OK");
    }
    else
    {
      alert("Ошибка при удалении!");
    }
  }
  else
  {
    alert("Выберите класс, который хотите удалить!");
  }
}

function deleteDomain(){
  var list_selected_property = $('#tree_property_for_rlation').treeview('getSelected');
  if (list_selected_property.length!=0)
  {
    var propertyname=list_selected_property[0]["text"]
    if(property_domain.getElementsByClassName("active").length!=0){
      var domainname = property_domain.getElementsByClassName("active")[0].id;
      var body = 'action=' + "deleteDomain" + '&propertyname=' + encodeURIComponent(propertyname) + '&domainname=' + encodeURIComponent(domainname);
      var resp = PostSyncRequest('/ontology/posthandler/', body);
      if (resp.status == "200")
      {
        displayDomainAndRange($('#tree_property_for_rlation').treeview('getSelected')[0]["text"]);
        console.log("OK");
      }
      else
      {
        alert("Ошибка при удалении!");
      }
    }
    else{
      alert("Выберите Domain для удаления!");
    }
  }
  else
  {
    alert("Выберите отношение!");
  }
}

function deleteRange(domainne){
  var list_selected_property = $('#tree_property_for_rlation').treeview('getSelected');
  if (list_selected_property.length!=0)
  {
    var propertyname=list_selected_property[0]["text"]
    if(property_range.getElementsByClassName("active").length!=0){
      var domainname = property_range.getElementsByClassName("active")[0].id;
      var body = 'action=' + "deleteRange" + '&propertyname=' + encodeURIComponent(propertyname) + '&rangename=' + encodeURIComponent(domainname);
      var resp = PostSyncRequest('/ontology/posthandler/', body);
      if (resp.status == "200")
      {
        displayDomainAndRange($('#tree_property_for_rlation').treeview('getSelected')[0]["text"]);
        console.log("OK");
      }
      else
      {
        alert("Ошибка при удалении!");
      }
    }
    else{
      alert("Выберите Range для удаления!");
    }
  }
  else
  {
    alert("Выберите отношение!");
  }
}

function deleteIndividual(thisa){
  if(list_individuals.getElementsByClassName("active").length!=0){
    var individualname = list_individuals.getElementsByClassName("active")[0].id;
    var body = 'action=' + "deleteIndividual" + '&individualname=' + encodeURIComponent(individualname);
    var resp = PostSyncRequest('/ontology/posthandler/', body);
    if (resp.status == "200")
    {
      displayIndividuals($('#treeClassIndividuals').treeview('getSelected')[0]["text"]);
      reload_class_concept
    }
    else
    {
      alert("Ошибка при удалении!");
    }
  }
  else{
    alert("Выберите экземпляр для удаления!");
  }
}

function deleteNewConceptRule(){
  if(list_new_concept_rules.getElementsByClassName("active").length!=0){
    var pk = list_new_concept_rules.getElementsByClassName("active")[0].id;
    var body = 'action=' + "deleteNewConceptRule" + '&pk=' + encodeURIComponent(pk);
    var resp = PostSyncRequest('/ontology/posthandler/', body);

    if (resp.status == "200")
    {
      displayNewConceptRule();
      console.log("OK");
    }
    else
    {
      alert("Ошибка при удалении!");
    }
  }
  else{
    alert("Выберите правило для удаления!");
  }
}

function deleteSubProcessRule(){
  if(list_sub_process_rules.getElementsByClassName("active").length!=0){
    var pk = list_sub_process_rules.getElementsByClassName("active")[0].id;
    var body = 'action=' + "deleteSubProcessRule" + '&pk=' + encodeURIComponent(pk);
    var resp = PostSyncRequest('/ontology/posthandler/', body);

    if (resp.status == "200")
    {
      displaySubProcessRule();
      console.log("OK");
    }
    else
    {
      alert("Ошибка при удалении!");
    }
  }
  else{
    alert("Выберите правило для удаления!");
  }
}


function displayNewConceptRule(){
  var a = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllNewConceptRules').response);
  var div = document.getElementById("list_new_concept_rules"); //получение div
  div.innerHTML="";
  var check_application_div=document.getElementById("form_applicationNewConceptRule");
  check_application_div.innerHTML="";
  for (var i = 0; i<a.length; i++)
  {
    var aa = document.createElement("a");
    aa.setAttribute("class", "list-group-item list-group-item-action");
    aa.setAttribute("data-toggle", "list");
    aa.setAttribute("href", "list-home");
    aa.setAttribute("role", "tab");
    aa.setAttribute("aria-controls", "home");
    aa.setAttribute("id", a[i][6])
    var p = document.createElement("p"); //Добавление пользователя
    var span = document.createElement("span");
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:red; color: black;");
    span.innerText="Пользователь";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][0];
    aa.appendChild(p);
    ///Добавление в применение
    var check_input = document.createElement("input");
    check_input.setAttribute("class","custom-control-input");
    check_input.setAttribute("type","checkbox");
    check_input.setAttribute("id", "customCheck_NewConcept"+i);
    var check_label= document.createElement("label");
    check_label.setAttribute("class","custom-control-label");
    check_label.setAttribute("for","customCheck_NewConcept"+i);
    var div_checkbox = document.createElement("div");

    p = document.createElement("p"); //Добавление даты
    span = document.createElement("span");
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:red; color: black;");
    span.innerText="Дата";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][5];
    aa.appendChild(p);

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:rgba(240, 240, 6, 0.822); color: black;");
    span.innerText="Класс 1";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][2];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerHTML+="Отношение 1";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][4];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:rgba(240, 240, 6, 0.822); color: black;");
    span.innerText="Класс 3";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][3];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    div_checkbox.appendChild(check_input);
    div_checkbox.appendChild(check_label);
    var img = document.createElement("img");
    img.setAttribute("src", "/media/"+a[i][1]);
    aa.appendChild(img);
    div.appendChild(aa);
    check_application_div.appendChild(div_checkbox);
  }
}

function displaySubProcessRule(){
  var a = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllSubProcessRules').response);
  var div = document.getElementById("list_sub_process_rules"); //получение div
  div.innerHTML="";
  var check_application_div=document.getElementById("form_applicationSubProcessRule");
  check_application_div.innerHTML="";
  for (var i = 0; i<a.length; i++)
  {
    var aa = document.createElement("a");
    aa.setAttribute("class", "list-group-item list-group-item-action");
    aa.setAttribute("data-toggle", "list");
    aa.setAttribute("href", "list-home");
    aa.setAttribute("role", "tab");
    aa.setAttribute("aria-controls", "home");
    aa.setAttribute("id", a[i][5])
    var p = document.createElement("p"); //Добавление пользователя
    var span = document.createElement("span");
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:red; color: black;");
    span.innerText="Пользователь";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][0];
    aa.appendChild(p);
    ///Добавление в применение
    var check_input = document.createElement("input");
    check_input.setAttribute("class","custom-control-input");
    check_input.setAttribute("type","checkbox");
    check_input.setAttribute("id", "customCheck_SubProcess"+i);
    var check_label= document.createElement("label");
    check_label.setAttribute("class","custom-control-label");
    check_label.setAttribute("for","customCheck_SubProcess"+i);
    var div_checkbox = document.createElement("div");

    p = document.createElement("p"); //Добавление даты
    span = document.createElement("span");
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:red; color: black;");
    span.innerText="Дата";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][4];
    aa.appendChild(p);

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerHTML+="Отношение 1";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][2];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerHTML+="Отношение 2";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][3];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    div_checkbox.appendChild(check_input);
    div_checkbox.appendChild(check_label);
    var img = document.createElement("img");
    img.setAttribute("src", "/media/"+a[i][1]);
    aa.appendChild(img);
    div.appendChild(aa);
    check_application_div.appendChild(div_checkbox);
  }
}

function displaySimpleRules()
{
  var a = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllSimpleRules').response);
  var div = document.getElementById("list_simple_rules"); //получение div
  div.innerHTML="";
  var check_application_div=document.getElementById("div_check_simple_rule");
  check_application_div.innerHTML="";
  for (var i = 0; i<a.length; i++)
  {
    var aa = document.createElement("a");
    aa.setAttribute("class", "list-group-item list-group-item-action");
    aa.setAttribute("data-toggle", "list");
    aa.setAttribute("href", "list-home");
    aa.setAttribute("role", "tab");
    aa.setAttribute("aria-controls", "home");
    aa.setAttribute("id", a[i][12])
    var p = document.createElement("p"); //Добавление пользователя
    var span = document.createElement("span");
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:red; color: black;");
    span.innerText="Пользователь";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][0];
    aa.appendChild(p);
    //Добавление в применение input и label
    var check_input = document.createElement("input");
    check_input.setAttribute("class","custom-control-input");
    check_input.setAttribute("type","checkbox");
    check_input.setAttribute("id", "customCheck"+i);
    var check_label= document.createElement("label");
    check_label.setAttribute("class","custom-control-label");
    check_label.setAttribute("for","customCheck"+i);
    var div_checkbox = document.createElement("div");
    //check_label.innerText+="<div class='row'>aaa</div><div class='row'>aaa</div>";
    /*var zzz=document.createElement("p");
    zzz.setAttribute("style", "background-color: yellow;")
    zzz.innerHTML+="aa";
    check_label.appendChild(zzz);
    var div_checkbox = document.createElement("div");
    div_checkbox.setAttribute("class","custom-control custom-checkbox");
    div_checkbox.appendChild(check_input);
    div_checkbox.appendChild(check_label);*/
    

    p = document.createElement("p"); //Добавление пользователя
    span = document.createElement("span");
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:red; color: black;");
    span.innerText="Дата";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][11];
    aa.appendChild(p);

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:rgba(240, 240, 6, 0.822); color: black;");
    span.innerText="Концепт 1";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][2];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true))

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerHTML+="Отношение 1";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][3];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerText="Направление 1";
    p.appendChild(span);
    var ii = document.createElement("i");
    ii.setAttribute("class", "fas fa-long-arrow-alt-right fa-2x");
    ii.setAttribute("style","color:#67c255;");
    if(a[i][4])
    {
      ii.setAttribute("class", "fas fa-long-arrow-alt-right fa-2x");
      ii.setAttribute("style","color:#67c255;");
    }
    else{
      ii.setAttribute("class", "fas fa-long-arrow-alt-left fa-2x");
      ii.setAttribute("style","color:#67c255;");
    }
    p.innerHTML+=" ";
    p.appendChild(ii);
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:rgba(240, 240, 6, 0.822); color: black;");
    span.innerText="Концепт 2";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][5];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerHTML+="Отношение 2";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][6];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerText="Направление 2";
    p.appendChild(span);
    var ii = document.createElement("i");
    ii.setAttribute("class", "fas fa-long-arrow-alt-right fa-2x");
    ii.setAttribute("style","color:#67c255;");
    if(a[i][7])
    {
      ii.setAttribute("class", "fas fa-long-arrow-alt-right fa-2x");
      ii.setAttribute("style","color:#67c255;");
    }
    else{
      ii.setAttribute("class", "fas fa-long-arrow-alt-left fa-2x");
      ii.setAttribute("style","color:#67c255;");
    }
    p.innerHTML+=" ";
    p.appendChild(ii);
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:rgba(240, 240, 6, 0.822); color: black;");
    span.innerText="Концепт 3";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][8];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerHTML+="Отношение 3";
    p.appendChild(span);
    p.innerHTML+=" "+a[i][9];
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    p = document.createElement("p"); //добавление параграфа
    span = document.createElement("span");
    span.setAttribute("class","badge");
    span.setAttribute("style","background-color:#67c255; color: black;");
    span.innerText="Направление 3";
    p.appendChild(span);
    var ii = document.createElement("i");
    ii.setAttribute("class", "fas fa-long-arrow-alt-right fa-2x");
    ii.setAttribute("style","color:#67c255;");
    if(a[i][10])
    {
      ii.setAttribute("class", "fas fa-long-arrow-alt-right fa-2x");
      ii.setAttribute("style","color:#67c255;");
    }
    else{
      ii.setAttribute("class", "fas fa-long-arrow-alt-left fa-2x");
      ii.setAttribute("style","color:#67c255;");
    }
    p.innerHTML+=" ";
    p.appendChild(ii);
    aa.appendChild(p);
    check_label.appendChild(p.cloneNode(true));

    div_checkbox.appendChild(check_input);
    div_checkbox.appendChild(check_label);
    var img = document.createElement("img");
    img.setAttribute("src", "/media/"+a[i][1]);
    aa.appendChild(img);
    div.appendChild(aa);
    check_application_div.appendChild(div_checkbox);
  }

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////                 РАБОТА С СЕКЦИЕЙ ОТНОШЕНИЙ           //////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function changeRelationHandler() //висит на изменении дивов
{
  var first_object = document.getElementById("first_object").options[document.getElementById("first_object").selectedIndex].text;
  let first_id = document.getElementById("first_object").options[document.getElementById("first_object").selectedIndex].getAttribute("id"); //получение id выбранного option
  var second_object = document.getElementById("second_object").options[document.getElementById("second_object").selectedIndex].text;
  let second_id = document.getElementById("second_object").options[document.getElementById("second_object").selectedIndex].getAttribute("id"); //получение id выбранного option
  
  var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+first_object+'&first_id='+first_id+'&second_object='+second_object+'&second_id='+second_id).response);
  var select_relation_property_object = document.getElementById("select_relation_property_object");
  select_relation_property_object.innerHTML = "";
  for (var i = 0; i<jsonStructClass.length; i++)
  {
    var option = document.createElement("option");
    option.setAttribute('id', jsonStructClass[i].split("#")[1]);
    option.setAttribute('name', jsonStructClass[i].split("#")[1]);
    option.textContent=jsonStructClass[i].split("#")[1];
    select_relation_property_object.append(option)
  }
}



function arrowClick(obj) //   меняет направление стрелки
{
  if (obj.dataset.right=="True")
  {
    obj.dataset.right="False";
    obj.setAttribute("class","fas fa-long-arrow-alt-left fa-7x");
  }
  else
  {
    obj.dataset.right="True";
    obj.setAttribute("class","fas fa-long-arrow-alt-right fa-7x");
  }
}
function changeSimpleRusesObject()
{
  var simple_rules_object_first = document.getElementById("simple_rules_object_first").options[document.getElementById("simple_rules_object_first").selectedIndex].text;
  var simple_rules_object_second = document.getElementById("simple_rules_object_second").options[document.getElementById("simple_rules_object_second").selectedIndex].text;
  var simple_rules_object_third = document.getElementById("simple_rules_object_third").options[document.getElementById("simple_rules_object_third").selectedIndex].text;
  if (raw_image1.dataset.right=="True") //если стрелка между 1 и 2 объектом вправо
  {
    var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+simple_rules_object_first+'&first_id=class'+'&second_object='+simple_rules_object_second+'&second_id=class').response);
  }
  else
  {
    if (raw_image1.dataset.right=="False")
    {
      var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+simple_rules_object_second+'&first_id=class'+'&second_object='+simple_rules_object_first+'&second_id=class').response);
    }
  }
  var select_relation_property_object = document.getElementById("simple_rules_property_first_second");
  select_relation_property_object.innerHTML = "";
  for (var i = 0; i<jsonStructClass.length; i++)
  {
    var option = document.createElement("option");
    option.setAttribute('id', jsonStructClass[i].split("#")[1]);
    option.setAttribute('name', jsonStructClass[i].split("#")[1]);
    option.textContent=jsonStructClass[i].split("#")[1];
    select_relation_property_object.append(option)
  }

  if (raw_image2.dataset.right=="True") //если стрелка между 1 и 2 объектом вправо
  {
    var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+simple_rules_object_second+'&first_id=class'+'&second_object='+simple_rules_object_third+'&second_id=class').response);
  }
  else
  {
    if (raw_image1.dataset.right=="False")
    {
      var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+simple_rules_object_third+'&first_id=class'+'&second_object='+simple_rules_object_second+'&second_id=class').response);
    }
  }
  var select_relation_property_object = document.getElementById("simple_rules_property_second_third");
  select_relation_property_object.innerHTML = "";
  for (var i = 0; i<jsonStructClass.length; i++)
  {
    var option = document.createElement("option");
    option.setAttribute('id', jsonStructClass[i].split("#")[1]);
    option.setAttribute('name', jsonStructClass[i].split("#")[1]);
    option.textContent=jsonStructClass[i].split("#")[1];
    select_relation_property_object.append(option)
  }

  if (raw_image3.dataset.right=="True") //если стрелка между 1 и 2 объектом вправо
  {
    var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+simple_rules_object_first+'&first_id=class'+'&second_object='+simple_rules_object_third+'&second_id=class').response);
  }
  else
  {
    if (raw_image1.dataset.right=="False")
    {
      var jsonStructClass = JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=GetRelationRestriction&first_object='+simple_rules_object_third+'&first_id=class'+'&second_object='+simple_rules_object_first+'&second_id=class').response);
    }
  }
  var select_relation_property_object = document.getElementById("simple_rules_property_first_third");
  select_relation_property_object.innerHTML = "";
  for (var i = 0; i<jsonStructClass.length; i++)
  {
    var option = document.createElement("option");
    option.setAttribute('id', jsonStructClass[i].split("#")[1]);
    option.setAttribute('name', jsonStructClass[i].split("#")[1]);
    option.textContent=jsonStructClass[i].split("#")[1];
    select_relation_property_object.append(option)
  }

  
}

function applicationSimpleRule() 
{
  //var form_application = applicationSimpleRule.querySelector("input");
  var list_checked_inputs = div_check_simple_rule.querySelectorAll("input:checked"); //получение выбранных правил
  if (list_checked_inputs.length == 0)
  {
    alert("Выберите правило!");
    return 0;
  }
  var list_of_check = [];
  for (var i =0; i<list_checked_inputs.length; i++)
  {
    list_of_check.push(list_checked_inputs[i].id.split("customCheck")[1]);
  }
  var body = 'action=' + "applicationSimpleRule" + '&list_num_of_rules='+encodeURIComponent(list_of_check);
  var resp = PostSyncRequest('/ontology/posthandler/', body);
  var jsonStructClass = JSON.parse(resp.response);
  if(jsonStructClass["result"]){
    displayReportSimpleRule(jsonStructClass["list_of_result"]);
    if(jsonStructClass["list_of_result"].length!=0){
      loadStructureClass();
      loadStructureProperty();
      reload_property();
      reload_class_concept();
    }
  }
  else
  {
    alert("Ошибка на сервере!");
  }
}
function displayReportSimpleRule(struct)
{
  var div_report=document.getElementById("report_simple_rule");
  div_report.innerHTML="";
  var header=document.createElement("div");
  header.setAttribute("class","text-center");
  header.setAttribute("style","background-color: #f02274;");
  var h4=document.createElement("h4");
  h4.innerHTML="Выведено "+struct.length + " отношений:";
  header.appendChild(h4);
  div_report.appendChild(header);
  if(struct.length!=0)
  {
    var div_list_of_report=document.createElement("div");
    div_list_of_report.setAttribute("class","row");
    for(var i=0; i<struct.length;i++)
    {
      var p = document.createElement("p"); //Добавление пользователя
      var span = document.createElement("span");
      span = document.createElement("span");
      span.setAttribute("class","badge");
      span.setAttribute("style","background-color:yellow; color: black;");
      span.innerText="Концепт 3 ";
      p.appendChild(span);
      p.innerHTML+=" "+struct[i][0]+" ";
      span = document.createElement("span");
      span.setAttribute("class","badge");
      span.setAttribute("style","background-color:yellow; color: black;");
      span.innerText="Концепт 1 ";
      p.appendChild(span);
      p.innerHTML+=" "+struct[i][1]+" ";
      div_list_of_report.appendChild(p); 
    }
    div_report.appendChild(div_list_of_report);
  }
}
function applicationNewConceptRule()
{

  var list_checked_inputs = form_applicationNewConceptRule.querySelectorAll("input:checked"); //получение выбранных правил
  if (list_checked_inputs.length == 0)
  {
    alert("Выберите правило!");
    return 0;
  }
  var list_of_check = [];
  for (var i =0; i<list_checked_inputs.length; i++)
  {
    list_of_check.push(list_checked_inputs[i].id.split("customCheck_NewConcept")[1]);
  }

  
  var body = 'action=' + "applicationNewConceptRuele" + '&list_num_of_rules='+encodeURIComponent(list_of_check);
  var resp = PostSyncRequest('/ontology/posthandler/', body)
  var jsonStructClass = JSON.parse(resp.response);
  if(jsonStructClass["result"]){
    displayReportNewConceptRule(jsonStructClass["list_of_result"]);
    if(jsonStructClass["list_of_result"].length!=0){
      loadStructureClass();
      loadStructureProperty();
      reload_property();
      reload_class_concept();
    }
  }
  else
  {
    alert("Ошибка на сервере!");
  }
}
function displayReportNewConceptRule(struct)
{
  var div_report=document.getElementById("report_new_concept");
  div_report.innerHTML="";
  var header=document.createElement("div");
  header.setAttribute("class","text-center");
  header.setAttribute("style","background-color: #f02274;");
  var h4=document.createElement("h4");
  h4.innerHTML="Выведено "+struct.length + " классов:";
  header.appendChild(h4);
  div_report.appendChild(header);
  if(struct.length!=0)
  {
    var div_list_of_report=document.createElement("div");
    div_list_of_report.setAttribute("class","row");
    for(var i=0; i<struct.length;i++)
    {
      var p = document.createElement("p"); //Добавление пользователя
      var span = document.createElement("span");
      span = document.createElement("span");
      span.setAttribute("class","badge");
      span.setAttribute("style","background-color:yellow; color: green;");
      span.innerText="Класс ";
      p.appendChild(span);
      p.innerHTML+=" "+struct[i]+" ";
      div_list_of_report.appendChild(p);
    }
    div_report.appendChild(div_list_of_report);
  }
}
function applicationSubProcessRule()
{
  var list_checked_inputs = form_applicationSubProcessRule.querySelectorAll("input:checked"); //получение выбранных правил
  if (list_checked_inputs.length == 0)
  {
    alert("Выберите правило!");
    return 0;
  }
  var list_of_check = [];
  for (var i =0; i<list_checked_inputs.length; i++)
  {
    list_of_check.push(list_checked_inputs[i].id.split("customCheck_SubProcess")[1]);
  }

  var body = 'action=' + "applicationSubProcessRule" + '&list_num_of_rules='+encodeURIComponent(list_of_check);
  var resp = PostSyncRequest('/ontology/posthandler/', body)
  var jsonStructClass = JSON.parse(resp.response);
  if(jsonStructClass["result"]){
    displayReportSubProcess(jsonStructClass["list_of_result"]);
    if(jsonStructClass["list_of_result"].length!=0){
      loadStructureClass();
      loadStructureProperty();
      reload_property();
      reload_class_concept();
    }
  }
  else
  {
    alert("Ошибка на сервере!");
  }
}
function displayReportSubProcess(struct){
  var div_report=document.getElementById("report_sub_process");
  div_report.innerHTML="";
  var header=document.createElement("div");
  header.setAttribute("class","text-center");
  header.setAttribute("style","background-color: #f02274;");
  var h4=document.createElement("h4");
  h4.innerHTML="Выведено "+struct.length + " отношений:";
  header.appendChild(h4);
  div_report.appendChild(header);
  if(struct.length!=0)
  {
    var div_list_of_report=document.createElement("div");
    div_list_of_report.setAttribute("class","row");
    for(var i=0; i<struct.length;i++)
    {
      var p = document.createElement("p"); //Добавление пользователя
      var span = document.createElement("span");
      span = document.createElement("span");
      span.setAttribute("class","badge");
      span.setAttribute("style","background-color:green; color: black;");
      span.innerText="Отношение ";
      p.appendChild(span);
      p.innerHTML+=" "+struct[i][1] + " ";
      //div_list_of_report.appendChild(p);
      
      //var p = document.createElement("p"); //Добавление пользователя
      var span = document.createElement("span");
      span = document.createElement("span");
      span.setAttribute("class","badge");
      span.setAttribute("style","background-color:green; color: black;");
      span.innerText="Domain ";
      p.appendChild(span);
      p.innerHTML+=" "+struct[i][0] + " ";
      //div_list_of_report.appendChild(p);

      //var p = document.createElement("p"); //Добавление пользователя
      var span = document.createElement("span");
      span = document.createElement("span");
      span.setAttribute("class","badge");
      span.setAttribute("style","background-color:green; color: black;");
      span.innerText="Range ";
      p.appendChild(span);
      p.innerHTML+=" "+struct[i][2];
      div_list_of_report.appendChild(p);
    }
    div_report.appendChild(div_list_of_report);
  }
}

function displayGraph(){
  window.open("/ontology/db_to_owl");
}

function getAllClasses(){
  return JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=ListOfAllClass').response);
}
function getAllIndividuals(){
  return JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllIndividual').response);
}
function getAllConcept(){
  return JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=Concept').response);
}
function setAttributes(el, attrs) {
  for(var key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
  return el;
}

function reload_class_concept(){ //Перезагрузка при добавлении и удалении классов
  var list_of_class = getAllClasses();
  var list_of_individual = getAllIndividuals();
  var list_of_concept=getAllConcept();
  list_class_and_individual_for_domain.innerHTML=""; //классы и экземпляры
  list_class_and_individual_for_range.innerHTML=""; //классы и экземпляры
  simple_rules_object_first.innerHTML="";
  simple_rules_object_second.innerHTML="";
  simple_rules_object_third.innerHTML="";
  setNewConceptRule_k1.innerHTML="";
  setNewConceptRule_k3.innerHTML="";
  for (var i=0; i<list_of_class.length;i++){
    if(list_of_class[i]=="#"){
      continue;
    }
    var option = document.createElement("option");
    option=setAttributes(option, {"id":list_of_class[i], "name":list_of_class[i], "style":"background-color: rgba(240, 240, 6, 0.822); color:black;"});
    option.innerHTML+=list_of_class[i];
    list_class_and_individual_for_domain.appendChild(option);
    list_class_and_individual_for_range.appendChild(option.cloneNode(true));
    setNewConceptRule_k1.appendChild(option.cloneNode(true));
    setNewConceptRule_k3.appendChild(option.cloneNode(true));
  }
  for (var i=0; i<list_of_individual.length;i++){
    var option = document.createElement("option");
    option=setAttributes(option, {"id":list_of_individual[i], "name":list_of_individual[i], "style":"background-color: #513166; color:black;"});
    option.innerHTML+=list_of_individual[i];
    list_class_and_individual_for_domain.appendChild(option);
    list_class_and_individual_for_range.appendChild(option.cloneNode(true));
  }
  for (var i=0; i<list_of_concept.length;i++){
    var option = document.createElement("option");
    option=setAttributes(option, {"id":list_of_concept[i], "name":list_of_concept[i], "style":"background-color: rgba(240, 240, 6, 0.822); color:black;"});
    option.innerHTML+=list_of_concept[i];
    simple_rules_object_first.appendChild(option);
    simple_rules_object_second.appendChild(option.cloneNode(true));
    simple_rules_object_third.appendChild(option.cloneNode(true));
  }
}

function getAllProperty(){
  return JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=AllProperty').response);
}

function getPropertyConcept(){
  return JSON.parse(GetSyncRequest('/ontology/getsrtuctureontology/?action=PropertyConcepts').response);
}

function reload_property(){
  var list_of_property = getAllProperty();
  var list_of_property_concepts = getPropertyConcept();
  //все свойства
  setNewConceptRule_p2.innerHTML="";
  setSubProcessRule_p2.innerHTML="";
  setSubProcessRule_p3.innerHTML="";
  //концепты
  simple_rules_property_first_second.innerHTML="";
  simple_rules_property_second_third.innerHTML="";
  simple_rules_property_first_third.innerHTML="";

  for (var i=0; i<list_of_property.length;i++){
    if(list_of_property[i]=="#"){
      continue;
    }
    var option = document.createElement("option");
    option=setAttributes(option, {"id":list_of_property[i], "name":list_of_property[i], "style":"background-color: #67c255; color: black;"});
    option.innerHTML+=list_of_property[i];
    setNewConceptRule_p2.appendChild(option);
    setSubProcessRule_p2.appendChild(option.cloneNode(true));
    setSubProcessRule_p3.appendChild(option.cloneNode(true));
  }
  for (var i=0; i<list_of_property_concepts.length;i++){
    if(list_of_property_concepts[i]=="#"){
      continue;
    }
    var option = document.createElement("option");
    option=setAttributes(option, {"id":list_of_property_concepts[i], "name":list_of_property_concepts[i], "style":"background-color: #67c255; color: black;"});
    option.innerHTML+=list_of_property_concepts[i];
    simple_rules_property_first_second.appendChild(option);
    simple_rules_property_second_third.appendChild(option.cloneNode(true));
    simple_rules_property_first_third.appendChild(option.cloneNode(true));
  }

}

function delete_ontology(){
  var body = 'action=' + "DeleteOntology";
  var resp = PostSyncRequest('/ontology/posthandler/', body);
  if (resp.status == "200")
  {
    document.location.href="../";
  }
  else
  {
    alert("Oшибка при удалении!");
  }
}
//////////////////////////// Обновление элементов //////////////////////////

$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
  
});

//////////////////////////////////////DISPLAY   GRAPH/////////////////////////////////


